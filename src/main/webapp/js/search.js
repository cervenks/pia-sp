$(function () {
  toggle_button();
});

$('#search_form\\:search_bar').on('input', function () {
    toggle_button();
});

function toggle_button() {
    var value = $('#search_form\\:search_bar').val();


    if(value.length >= 3) {
        call();
    }
}

function call() {
    var button = $("#search_form\\:search_button");
    button.click();
}
$(function() {
    jquerySelectors();
    setInterval(function() {
        refresh();
    }, 10000);
});

function refresh() {
    $("#refresh_form\\:refresh_button").click();
}

function onNewPostSent() {
    jquerySelectors();
    $("#new_post_form\\:new_post_input").val("");
}

function jquerySelectors() {
    $(".modal_toggle").click(function() {
        var modalToShow = $(this).attr('data-target');
        $(modalToShow).modal('show');
    });
}

$(window).scroll(function() {
    let scroll = ($(window).scrollTop() + $(window).height() | 0);
    let bottom = $(document).height();
    if(scroll == bottom) {
        $("#show_more_form\\:show_more_button").click();
    }
});


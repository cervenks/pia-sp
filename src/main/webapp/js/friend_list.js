var onlineFriends = {};
var count = 0;
var openedChatModal = -1;
var openedChatEmail = null;


$(function () {
    connect();
    requestsOnlineFriends();
});


function chatButtonSelector() {
    $(".chat_button").click(function() {
        var email = $(this).attr('data-email');
        var info = onlineFriends[email];
        onChatOpen(info);
        $("#chat_modal").modal('show');
    });
}

$('#chat_modal').on('hidden.bs.modal', function (e) {
    openedChatModal = -1;
    openedChatEmail = null;
});

$('#message_send_button').click(function() {
   sendNewMessage();
   $("#message_send_input").val("");
});


function connect() {
    var socket = new SockJS('/stomp');
    var headers = getHeaders();
    var friendUrl = getFriendQueueAddress();
    var messageUrl = getMessageQueueAddress();
    stompClient = Stomp.over(socket);
    stompClient.connect(headers, function (frame) {
        console.log('Connected: ' + frame);
        stompClient.subscribe(friendUrl, function (message) {
            var info = JSON.parse(message.body);
            proceedFriendMessage(info);
        });
        stompClient.subscribe(messageUrl, function(message) {
            var info = JSON.parse(message.body);
            proceedNewMessage(info);
        });
    });
}

function requestsOnlineFriends() {
    var url = '/api/online_friends';

    $.get(url, function(message) {
        displayFriends(message);
        chatButtonSelector();
    });


}

function sendNewMessage() {
    var text = $("#message_send_input").val();
    var headers = getHeaders();
    var url = "/api/chat/send";
    var value = {'whom': openedChatEmail, 'text': text};

    $.ajax({
        type: 'PUT',
        url: url,
        data: JSON.stringify(value),
        headers: headers,
        contentType: 'application/json',
        dataType: 'json'
    });
}

function proceedFriendMessage(info) {
    if(info['authType'] == 'LOGIN') {
        onLogin(info['userEmail'], info['username']);
    }

    if(info['authType'] == 'LOGOUT') {
        onLogout(info['userEmail']);
    }
}

function proceedNewMessage(info) {
    onNewMessage(info);
}

function onLogin(email, username) {
    var info = addToArray(email, username);
    makeUserHtml(info);
    chatButtonSelector();
}

function onLogout(email) {
    console.log(onlineFriends);
    var info = onlineFriends[email];
    removeUserHtml(info['id']);
    delete onlineFriends[email];
}

function onNewMessage(message) {
    var otherUser = message['otherUserEmail'];
    var info = onlineFriends[otherUser];
    var id = info['id'];

    if(openedChatModal === id) {
        appendChat(message);
    } else {
        toggleNewMessageBadge(info);
    }
}

function onChatOpen(info) {
    displayChatHistory(info['email']);

    resetNewMessages(info);
    openedChatModal = info['id'];
    openedChatEmail = info['email'];
    $("#chat_title").html("Chat: " + openedChatEmail);
}

function displayChatHistory(email) {
    var url = "/api/chat/history/" + email;
    $.get(url, function(messages) {
        $(".chat").html("");
        messages.forEach(function(message) {
            appendChat(message);
        });
    });
}

function toggleNewMessageBadge(info) {
    info['new_messages']++;
    displayNewMessages(info);
}

function resetNewMessages(info) {
    info['new_messages'] = 0;
    displayNewMessages(info);
}

function addToArray(email, username) {
    var info = {'id': count, 'email': email, 'username': username, 'new_messages': 0,};
    onlineFriends[email] = info;
    count++;
    return info;
}

function makeUserHtml(info) {
    $("#online-friends").append("<button id=\"friend_button"+ info['id'] +"\" class=\"list-group-item list-group-item-action " +
        "d-flex justify-content-between align-items-center chat_button\" data-email=\""+info['email']+"\">" +
        info['username'] +
        "<span id=\"badge"+ info['id'] +"\" class=\"badge bg-danger rounded-pill\"></span>" +
        "</button>");
}

function removeUserHtml(id) {
    var elementName = "#friend_button" + id;
    $(elementName).remove();
}

function displayNewMessages(info) {
    var elementId = "#badge" + info['id'];
    var new_messages = info['new_messages'];
    if(new_messages === 0) {
        $(elementId).html("");
    } else {
        $(elementId).html(new_messages);
    }

}



function displayFriends(users) {
    users.forEach(function(user) {
        onLogin(user, user);
    });
}

function appendChat(message) {
    if(message['sentByMe']) {
        appendChatByMe(message);
    } else {
        appendChatByHim(message);
    }
}

function appendChatByMe(message) {
    var user = $("#current_user").val();
    $(".chat").append("<li>" +
        "<div class=\"chat-body me-3\">" +
        "<div class=\"d-flex justify-content-between\">" +
        "<strong class='text-primary'>"+ user +"</strong>" +
        "<small class=\" text-muted\">"+message['timestamp']+"</small>" +
        "</div>" +
        "<p style=\"text-align: justify;\">" +
        message['text'] +
        "</p>" +
        "</div>" +
        "</li>");
}


function appendChatByHim(message) {
    $(".chat").append("<li>" +
        "<div class=\"chat-body me-3\">" +
        "<div class=\"d-flex justify-content-between\">" +
        "<strong class='text-secondary'>" + openedChatEmail + "</strong>" +
        "<small class=\"text-muted\">"+message['timestamp']+"</small>" +
        "</div>" +
        "<p style=\"text-align: justify;\">" +
        message['text'] +
        "</p>" +
        "</div>" +
        "</li>");
}

function getFriendQueueAddress() {
    var currentUser = getCurrentUser();
    return '/user/'+ currentUser +'/queue/friends';
}

function getMessageQueueAddress() {
    var currentUser = getCurrentUser();
    return '/user/' + currentUser + '/queue/messages';
}

function getCurrentUser() {
    return $("#current_user").val();
}
$("#form_register\\:email_input").change(function () {
    var inputElement = $('#form_register\\:email_input')
    var validationElement = $('#email_validation_message');
    var url = '/api/validate/email';
    var value = inputElement.val();

    validate(url, inputElement, validationElement, value);
});

$("#form_register\\:username_input").change(function () {
    var inputElement = $('#form_register\\:username_input')
    var validationElement = $('#username_validation_message');
    var url = '/api/validate/username';
    var value = inputElement.val();

    validate(url, inputElement, validationElement, value);
});

$(".password_input").change(function () {
    var inputElement = $('.password_input')
    var validationElement = $('#password_validation_message');
    var url = '/api/validate/password';
    var value = JSON.stringify({
        'password': $('#form_register\\:password_input').val(),
        'passwordCheck': $('#form_register\\:password_check_input').val()
    });

    validate(url, inputElement, validationElement, value);
});

function validate(url, inputElement, validationElement, value) {
    var headers = getHeaders();

    $.ajax({
        type: 'POST',
        url: url,
        data: value,
        headers: headers,
        contentType: 'application/json',
        dataType: 'json',
        success: [ function (response) {
            if(response['valid']) {
                setValid(inputElement,  validationElement);
            } else {
                setInvalid(inputElement, validationElement);
            }
            validationElement.html(response['validationMessage']);
        } ]
    });
}

function setValid(inputElement, validationElement) {
    inputElement.removeClass('is-invalid');
    inputElement.addClass('is-valid');
    validationElement.removeClass('invalid-feedback');
    validationElement.addClass('valid-feedback');
}

function setInvalid(inputElement, validationElement) {
    inputElement.removeClass('is-valid');
    inputElement.addClass('is-invalid');
    validationElement.removeClass('valid-feedback');
    validationElement.addClass('invalid-feedback');
}
package cz.cervenks.pia.application.data.entity.factory.impl;

import cz.cervenks.pia.application.data.entity.RelationshipEntity;
import cz.cervenks.pia.application.data.entity.RelationshipStateEntity;
import cz.cervenks.pia.application.data.entity.UserEntity;
import cz.cervenks.pia.application.data.entity.factory.RelationshipFactory;
import cz.cervenks.pia.application.data.entity.factory.RelationshipStateFactory;
import cz.cervenks.pia.application.data.entity.factory.UserEntityFactory;
import cz.cervenks.pia.application.data.repository.RelationshipRepository;
import cz.cervenks.pia.business.domain.RelationshipState;
import cz.cervenks.pia.business.exception.InvalidDatabaseOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component("friendshipFactory")
@RequiredArgsConstructor
public class RelationshipFactoryImpl implements RelationshipFactory {

    private final RelationshipRepository relationshipRepository;
    private final RelationshipStateFactory relationshipStateFactory;
    private final UserEntityFactory userEntityFactory;

    @SneakyThrows
    @Override
    public RelationshipEntity getRelationshipEntityFromDatabase(String userEmail1, String userEmail2) {
        //use only if relationship exists
        Optional<RelationshipEntity> queryResult = relationshipRepository.findByUser1EmailAndUser2Email(userEmail1, userEmail2);
        if(queryResult.isEmpty()) {
            throw new InvalidDatabaseOperation("Relationship between " + userEmail1 + " and " + userEmail2 + " not found.");
        } else {
            return queryResult.get();
        }
    }

    @Override
    public RelationshipEntity makeRelationshipEntity(UserEntity userEntity1, UserEntity userEntity2, RelationshipState state) {
        RelationshipEntity relationshipEntity = new RelationshipEntity();
        RelationshipStateEntity relationshipStateEntity =
                relationshipStateFactory.getOrCreateNewEntity(state);
        relationshipEntity.setUser1(userEntity1);
        relationshipEntity.setUser2(userEntity2);
        relationshipEntity.setRelationshipState(relationshipStateEntity);
        return relationshipEntity;
    }

    @Override
    public RelationshipEntity makeRelationshipEntity(String userEmail1, String userEmail2, RelationshipState state) {
        UserEntity userEntity1 = userEntityFactory.getUserEntityFromDatabase(userEmail1);
        UserEntity userEntity2 = userEntityFactory.getUserEntityFromDatabase(userEmail2);
        return makeRelationshipEntity(userEntity1, userEntity2, state);
    }
}

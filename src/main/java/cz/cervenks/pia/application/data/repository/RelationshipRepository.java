package cz.cervenks.pia.application.data.repository;

import cz.cervenks.pia.application.data.entity.RelationshipEntity;
import cz.cervenks.pia.application.data.entity.RelationshipStateEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface RelationshipRepository extends CrudRepository<RelationshipEntity, UUID> {

    Optional<RelationshipEntity> findByUser1EmailAndUser2Email(
            String userEmail1, String userEmail2
    );

    List<RelationshipEntity> findByUser1EmailAndRelationshipStateIn(
            String userEmail, Collection<RelationshipStateEntity> states);

    List<RelationshipEntity> findByUser1Email(String userEmail);
}

package cz.cervenks.pia.application.data.repository;

import cz.cervenks.pia.application.data.entity.RoleEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RoleRepository extends CrudRepository<RoleEntity, UUID> {

    Optional<RoleEntity> findByTitle(String title);
}

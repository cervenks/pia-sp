package cz.cervenks.pia.application.data.entity.factory;

import cz.cervenks.pia.application.data.entity.RelationshipStateEntity;
import cz.cervenks.pia.business.domain.RelationshipState;

public interface RelationshipStateFactory {

    RelationshipStateEntity getOrCreateNewEntity(RelationshipState relationShipState);
}

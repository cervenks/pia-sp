package cz.cervenks.pia.application.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "auth_user")
@Data
public class UserEntity {

    @Id
    @GeneratedValue
    private UUID id;


    @Column(nullable = false)
    private String username;


    @Column(nullable = false)
    private String email;


    @Column(nullable = false)
    private String password;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "auth_userrole",
            joinColumns = {
                    @JoinColumn(name = "user_id")
            },
            inverseJoinColumns = {
                    @JoinColumn(name = "role_id")
            }
    )
    private Set<RoleEntity> roles = Collections.emptySet();


    @OneToMany(mappedBy = "user1", fetch = FetchType.LAZY)
    private Set<RelationshipEntity> relationships = Collections.emptySet();

    //throws stack overflow if default
    @Override
    public String toString() {
        return "User: " + email;
    }


}

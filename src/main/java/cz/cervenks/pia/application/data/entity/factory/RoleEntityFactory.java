package cz.cervenks.pia.application.data.entity.factory;

import cz.cervenks.pia.application.data.entity.RoleEntity;
import cz.cervenks.pia.business.domain.Role;

public interface RoleEntityFactory {

    RoleEntity getOrCreateNewEntity(Role role);
}

package cz.cervenks.pia.application.data.model;

import cz.cervenks.pia.application.data.entity.*;
import cz.cervenks.pia.application.data.entity.factory.LikeEntityFactory;
import cz.cervenks.pia.application.data.entity.factory.PostEntityFactory;
import cz.cervenks.pia.application.data.entity.factory.RelationshipStateFactory;
import cz.cervenks.pia.application.data.entity.factory.UserEntityFactory;
import cz.cervenks.pia.application.data.mapper.Mapper;
import cz.cervenks.pia.application.data.repository.LikeRepository;
import cz.cervenks.pia.application.data.repository.PostRepository;
import cz.cervenks.pia.application.data.repository.RelationshipRepository;
import cz.cervenks.pia.business.domain.Post;
import cz.cervenks.pia.business.domain.RelationshipState;
import cz.cervenks.pia.business.model.PostModel;
import cz.cervenks.pia.business.usecases.feed.result.PostInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component("postModel")
@RequiredArgsConstructor
public class PostModelImpl implements PostModel {

    private final PostRepository postRepository;
    private final LikeRepository likeRepository;
    private final RelationshipRepository relationshipRepository;

    private final UserEntityFactory userEntityFactory;
    private final PostEntityFactory postEntityFactory;
    private final LikeEntityFactory likeEntityFactory;

    private final RelationshipStateFactory relationshipStateFactory;
    private final Mapper mapper;

    @Override
    public PostInfo getPost(String userEmail, UUID uuid) {
        PostEntity postEntity = postEntityFactory.getPostEntityFromDatabase(uuid);
        List<LikeEntity> likeEntities = likeRepository.findByPost(postEntity);

        return mapper.mapToPostInfo(userEmail, postEntity, likeEntities);
    }

    public List<PostInfo> getPostsFor(String userEmail, Timestamp timestamp, int limit) {

        List<UserEntity> postsFrom = constructUserList(userEmail);
        return constructPosts(userEmail, postsFrom, timestamp, limit);
    }

    @Override
    public void addPost(Post post) {
        PostEntity postEntity = postEntityFactory.makePostEntity(post);
        postRepository.save(postEntity);
    }

    @Override
    public void toggleLike(UUID postId, String userEmail) {
        Optional<LikeEntity> queryResult = likeRepository.findByPostIdAndUserEmail(postId, userEmail);
        //if like exists, remove it and vice versa
        if(queryResult.isEmpty()) {
            createLike(postId, userEmail);
        } else {
            LikeEntity likeEntity = queryResult.get();
            removeLike(likeEntity);
        }
    }

    private void removeLike(LikeEntity likeEntity) {
        likeRepository.delete(likeEntity);
    }

    private void createLike(UUID postId, String userEmail) {
        LikeEntity likeEntity = likeEntityFactory.makeLikeEntity(userEmail, postId);
        likeRepository.save(likeEntity);
    }

    private List<UserEntity> constructUserList(String email) {
        //posts to display for one user
        //it contains himself and all his friends
        List<UserEntity> result = new ArrayList<>();
        addCurrentUser(result, email);
        addFriends(result, email);
        return result;
    }

    private List<PostInfo> constructPosts(String userEmail, List<UserEntity> postsFrom, Timestamp timestamp, int limit) {
        //timestamp is the time user opened feed page
        //we will add n older posts and all newer posts
        //(according to assignment new posts must not replace the old one)
        List<PostEntity> result = postRepository.getNewerPosts(postsFrom, timestamp);
        List<PostEntity> olderPosts = postRepository.getOlderPosts(postsFrom, timestamp, limit);
        result.addAll(olderPosts);
        return mapPosts(userEmail, result);
    }

    private void addCurrentUser(List<UserEntity> result, String email) {
        UserEntity userEntity = userEntityFactory.getUserEntityFromDatabase(email);
        result.add(userEntity);
    }

    private void addFriends(List<UserEntity> result, String userEmail) {
        List<RelationshipStateEntity> desiredStates = mapState(RelationshipState.FRIENDS);
        List<RelationshipEntity> relationships = relationshipRepository.findByUser1EmailAndRelationshipStateIn(
                userEmail, desiredStates
        );
        extractUsers(result, relationships);
    }

    private List<RelationshipStateEntity> mapState(RelationshipState state) {
        //we want to get all friends and must provide the list of relationship state entity
        //this method will map it
        List<RelationshipStateEntity> result = new ArrayList<>();
        RelationshipStateEntity stateEntity = relationshipStateFactory.getOrCreateNewEntity(state);
        result.add(stateEntity);
        return result;
    }

    private void extractUsers(List<UserEntity> result, List<RelationshipEntity> relationshipEntities) {
        //extract users from friends entities
        for(RelationshipEntity relationshipEntity : relationshipEntities) {
            UserEntity user = relationshipEntity.getUser2();
            result.add(user);
        }
    }

    private List<PostInfo> mapPosts(String userEmail, List<PostEntity> postEntities) {
        List<PostInfo> result = new ArrayList<>();
        for(PostEntity postEntity : postEntities) {
            //unfortunately Set<LikeEntity> in PostEntity throws Stack Overflow Exception
            List<LikeEntity> likeEntities = likeRepository.findByPost(postEntity);
            PostInfo postInfo = mapper.mapToPostInfo(userEmail, postEntity, likeEntities);
            result.add(postInfo);
        }
        return result;
    }

}

package cz.cervenks.pia.application.data.entity.factory.impl;

import cz.cervenks.pia.application.data.entity.MessageEntity;
import cz.cervenks.pia.application.data.entity.UserEntity;
import cz.cervenks.pia.application.data.entity.factory.MessageEntityFactory;
import cz.cervenks.pia.application.data.entity.factory.UserEntityFactory;
import cz.cervenks.pia.business.domain.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component("messageEntityFactory")
@RequiredArgsConstructor
public class MessageEntityFactoryImpl implements MessageEntityFactory {

    private final UserEntityFactory userEntityFactory;

    @Override
    public MessageEntity makeMessageEntity(Message message) {
        Timestamp timestamp = message.getTimestamp();
        String text = message.getText();
        String senderEmail = message.getSenderEmail();
        String receiverEmail = message.getReceiverEmail();

        UserEntity senderEntity = userEntityFactory.getUserEntityFromDatabase(senderEmail);
        UserEntity receiverEntity = userEntityFactory.getUserEntityFromDatabase(receiverEmail);

        MessageEntity result = new MessageEntity();
        result.setSender(senderEntity);
        result.setReceiver(receiverEntity);
        result.setTimestamp(timestamp);
        result.setText(text);
        return result;
    }
}

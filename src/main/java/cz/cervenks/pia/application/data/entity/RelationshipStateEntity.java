package cz.cervenks.pia.application.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;


@Entity
@Table(name = "relationship_state")
@Data
public class RelationshipStateEntity {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(nullable = false)
    private String title;
}

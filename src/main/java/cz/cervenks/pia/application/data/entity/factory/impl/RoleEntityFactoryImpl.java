package cz.cervenks.pia.application.data.entity.factory.impl;

import cz.cervenks.pia.application.data.entity.RoleEntity;
import cz.cervenks.pia.application.data.entity.factory.RoleEntityFactory;
import cz.cervenks.pia.application.data.repository.RoleRepository;
import cz.cervenks.pia.business.domain.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component("roleEntityFactory")
@RequiredArgsConstructor
public class RoleEntityFactoryImpl implements RoleEntityFactory {

    private final RoleRepository roleRepository;

    @Override
    public RoleEntity getOrCreateNewEntity(Role role) {
        String title = role.name();
        Optional<RoleEntity> queryResult = roleRepository.findByTitle(title);

        //if the entity is not present in query result,
        //we will call method create new entity a return result
        return queryResult.orElseGet(() -> createNewEntity(title));
    }

    private RoleEntity createNewEntity(String title) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setTitle(title);

        roleRepository.save(roleEntity);
        return roleEntity;
    }


}

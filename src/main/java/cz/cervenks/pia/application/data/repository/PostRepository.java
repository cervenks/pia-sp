package cz.cervenks.pia.application.data.repository;

import cz.cervenks.pia.application.data.entity.PostEntity;
import cz.cervenks.pia.application.data.entity.UserEntity;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public interface PostRepository extends CrudRepository<PostEntity, UUID> {



    @Query("SELECT pe " +
            " FROM PostEntity pe " +
            " WHERE (pe.user IN :postsFrom OR pe.isAnnouncement = true) AND pe.timestamp > :timestamp" +
            " ORDER BY pe.timestamp DESC" +
            ""
    )
    List<PostEntity> getNewerPosts(Collection<UserEntity> postsFrom, Timestamp timestamp);


    default List<PostEntity> getOlderPosts(Collection<UserEntity> postsFrom, Timestamp timestamp, int limit) {
        return getOlderPosts(postsFrom, timestamp, PageRequest.of(0, limit));
    }

    @Query("SELECT pe " +
            " FROM PostEntity pe " +
            " WHERE (pe.user IN :postsFrom OR pe.isAnnouncement = true) AND pe.timestamp <= :timestamp" +
            " ORDER BY pe.timestamp DESC" +
            ""
    )
    List<PostEntity> getOlderPosts(Collection<UserEntity> postsFrom, Timestamp timestamp, Pageable pageable);

    @Query("SELECT COUNT(pe) " +
            " FROM PostEntity pe " +
            " WHERE (pe.user IN :postsFrom OR pe.isAnnouncement = true) AND pe.timestamp < :timestamp" +
            " ORDER BY pe.timestamp DESC" +
            ""
    )
    long countOlder();

    @Query("SELECT COUNT(pe) " +
            " FROM PostEntity pe " +
            " WHERE (pe.user IN :postsFrom OR pe.isAnnouncement = true) AND pe.timestamp > :timestamp" +
            " ORDER BY pe.timestamp DESC" +
            ""
    )
    long countNewer();




}

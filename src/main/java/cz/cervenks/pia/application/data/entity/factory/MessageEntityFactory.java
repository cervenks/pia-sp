package cz.cervenks.pia.application.data.entity.factory;

import cz.cervenks.pia.application.data.entity.MessageEntity;
import cz.cervenks.pia.business.domain.Message;

public interface MessageEntityFactory {
    MessageEntity makeMessageEntity(Message message);
}

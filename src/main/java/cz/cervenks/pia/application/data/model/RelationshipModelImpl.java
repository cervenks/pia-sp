package cz.cervenks.pia.application.data.model;

import cz.cervenks.pia.application.data.entity.RelationshipEntity;
import cz.cervenks.pia.application.data.entity.RelationshipStateEntity;
import cz.cervenks.pia.application.data.entity.UserEntity;
import cz.cervenks.pia.application.data.entity.factory.RelationshipFactory;
import cz.cervenks.pia.application.data.entity.factory.RelationshipStateFactory;
import cz.cervenks.pia.application.data.entity.factory.UserEntityFactory;
import cz.cervenks.pia.application.data.mapper.Mapper;
import cz.cervenks.pia.application.data.repository.RelationshipRepository;
import cz.cervenks.pia.business.domain.RelationshipState;
import cz.cervenks.pia.business.domain.User;
import cz.cervenks.pia.business.model.RelationshipModel;
import cz.cervenks.pia.business.usecases.relationship.result.UserWithState;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Component("friendshipModel")
@Slf4j
public class RelationshipModelImpl implements RelationshipModel {

    private final UserEntityFactory userEntityFactory;
    private final RelationshipStateFactory relationshipStateFactory;

    private final RelationshipRepository relationshipRepository;
    private final RelationshipFactory relationshipFactory;

    private final Mapper mapper;


    @Override
    public List<UserWithState> getUsersByState(String userEmail, Collection<RelationshipState> states) {
        //get all users that are with current user with specified relationship (one or more relation state)
        List<RelationshipStateEntity> entityStateList = mapStates(states);
        List<RelationshipEntity> entityList = relationshipRepository
                .findByUser1EmailAndRelationshipStateIn(userEmail, entityStateList);
        return createUserList(entityList);

    }

    @Override
    public List<UserWithState> getUsersByState(String userEmail, RelationshipState state) {
        //get all users that are with current user with specified relationship (only one relation state)
        List<RelationshipStateEntity> entityStateList = mapState(state);
        List<RelationshipEntity> entityList = relationshipRepository
                .findByUser1EmailAndRelationshipStateIn(userEmail, entityStateList);
        return createUserList(entityList);
    }

    @Override
    public List<UserWithState> getAllUsersWithRelation(String userEmail) {
        List<RelationshipEntity> entityList = relationshipRepository.findByUser1Email(userEmail);
        return createUserList(entityList);
    }

    @Override
    public RelationshipState getState(String userEmail1, String userEmail2) {
        Optional<RelationshipEntity> queryResult = relationshipRepository.
                findByUser1EmailAndUser2Email(userEmail1, userEmail2);
        return getStateFromQueryResult(queryResult);
    }

    @Override
    public void setBidirectionalState(String userEmail1, String userEmail2, RelationshipState targetState) {
        //set state between two users
        //it must be mirror state (for example INVOKED_BY_ME and INVOKED_BY_HIM)
        RelationshipState mirrorState = targetState.getMirrorState();

        setUnidirectionalState(userEmail1, userEmail2, targetState);
        setUnidirectionalState(userEmail2, userEmail1, mirrorState);
    }

    private List<RelationshipStateEntity> mapStates(Collection<RelationshipState> states) {
        List<RelationshipStateEntity> result = new ArrayList<>();
        for(RelationshipState state : states) {
            RelationshipStateEntity stateEntity = relationshipStateFactory.getOrCreateNewEntity(state);
            result.add(stateEntity);
        }
        return result;
    }

    private List<RelationshipStateEntity> mapState(RelationshipState state) {
        List<RelationshipStateEntity> result = new ArrayList<>();
        RelationshipStateEntity stateEntity = relationshipStateFactory.getOrCreateNewEntity(state);
        result.add(stateEntity);
        return result;
    }

    private List<UserWithState> createUserList(List<RelationshipEntity> relationshipEntityList) {
        List<UserWithState> result = new ArrayList<>();
        for(RelationshipEntity relationshipEntity : relationshipEntityList) {
            UserWithState user = createUserWithState(relationshipEntity);
            result.add(user);
        }
        return result;
    }

    private UserWithState createUserWithState(RelationshipEntity relationshipEntity) {
        UserEntity userEntity = relationshipEntity.getUser2();
        User user = mapper.mapToUser(userEntity);
        RelationshipStateEntity stateEntity = relationshipEntity.getRelationshipState();
        RelationshipState state = mapper.mapToStatus(stateEntity);
        return new UserWithState(user, state);
    }

    private RelationshipState getStateFromQueryResult(Optional<RelationshipEntity> queryResult) {
        if(queryResult.isEmpty()) {
            return RelationshipState.NO_RELATIONSHIP;
        }

        RelationshipEntity relationshipEntity = queryResult.get();
        RelationshipStateEntity relationshipStateEntity = relationshipEntity.getRelationshipState();
        return mapper.mapToStatus(relationshipStateEntity);
    }

    private void setUnidirectionalState(String userEmail1, String userEmail2, RelationshipState targetState) {
        Optional<RelationshipEntity> queryResult = relationshipRepository.
                findByUser1EmailAndUser2Email(userEmail1, userEmail2);

        if(queryResult.isEmpty()) {
            createNewRelationship(userEmail1, userEmail2, targetState);
        } else {
            RelationshipEntity relationshipEntity = queryResult.get();
            updateRelationship(relationshipEntity, targetState);
        }
    }

    private void createNewRelationship(String userEmail1, String userEmail2, RelationshipState targetState) {
        if(targetState == RelationshipState.NO_RELATIONSHIP) {
            //we will create nothing
            return;
        }

        RelationshipEntity relationshipEntity = relationshipFactory.makeRelationshipEntity(userEmail1, userEmail2, targetState);
        relationshipRepository.save(relationshipEntity);
    }

    private void updateRelationship(RelationshipEntity relationshipEntity, RelationshipState targetState) {
        if(targetState == RelationshipState.NO_RELATIONSHIP) {
            relationshipRepository.delete(relationshipEntity);
            return;
        }

        RelationshipStateEntity stateEntity = relationshipStateFactory.getOrCreateNewEntity(targetState);
        relationshipEntity.setRelationshipState(stateEntity);
        relationshipRepository.save(relationshipEntity);
    }


}

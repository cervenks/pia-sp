package cz.cervenks.pia.application.data.repository;

import cz.cervenks.pia.application.data.entity.LikeEntity;
import cz.cervenks.pia.application.data.entity.PostEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface LikeRepository extends CrudRepository<LikeEntity, UUID> {

    List<LikeEntity> findByPost(PostEntity postEntity);
    Optional<LikeEntity> findByPostIdAndUserEmail(UUID postId, String userEmail);


}

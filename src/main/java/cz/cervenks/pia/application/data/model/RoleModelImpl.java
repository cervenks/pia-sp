package cz.cervenks.pia.application.data.model;

import cz.cervenks.pia.application.data.entity.RoleEntity;
import cz.cervenks.pia.application.data.entity.UserEntity;
import cz.cervenks.pia.application.data.entity.factory.RoleEntityFactory;
import cz.cervenks.pia.application.data.entity.factory.UserEntityFactory;
import cz.cervenks.pia.application.data.repository.UserRepository;
import cz.cervenks.pia.business.domain.Role;
import cz.cervenks.pia.business.model.RoleModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@RequiredArgsConstructor
public class RoleModelImpl implements RoleModel {

    private final UserEntityFactory userEntityFactory;
    private final RoleEntityFactory roleEntityFactory;
    private final UserRepository userRepository;

    @Override
    public boolean hasRole(String userEmail, Role role) {
        UserEntity userEntity = userEntityFactory.getUserEntityFromDatabase(userEmail);
        RoleEntity roleEntity = roleEntityFactory.getOrCreateNewEntity(role);
        return getUserRoleEntity(userEntity, roleEntity) != null;
    }

    @Override
    public boolean addRole(String userEmail, Role role) {
        UserEntity userEntity = userEntityFactory.getUserEntityFromDatabase(userEmail);
        RoleEntity roleEntity = roleEntityFactory.getOrCreateNewEntity(role);
        if (getUserRoleEntity(userEntity, roleEntity) == null) {
            userEntity.getRoles().add(roleEntity);
            userRepository.save(userEntity);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean removeRole(String userEmail, Role role) {
        UserEntity userEntity = userEntityFactory.getUserEntityFromDatabase(userEmail);
        RoleEntity roleEntity = roleEntityFactory.getOrCreateNewEntity(role);
        RoleEntity userRoleEntity = getUserRoleEntity(userEntity, roleEntity);
        if(userRoleEntity != null) {
            userEntity.getRoles().remove(userRoleEntity);
            userRepository.save(userEntity);
            return true;
        } else {
            return false;
        }
    }

    private RoleEntity getUserRoleEntity(UserEntity userEntity, RoleEntity roleEntity) {
        UUID roleEntityId = roleEntity.getId();

        for(RoleEntity userRole : userEntity.getRoles()) {
            UUID userRoleId = userRole.getId();
            if(userRoleId.equals(roleEntityId)) {
                return userRole;
            }
        }
        return null;
    }
}

package cz.cervenks.pia.application.data.entity.factory.impl;

import cz.cervenks.pia.application.data.entity.LikeEntity;
import cz.cervenks.pia.application.data.entity.PostEntity;
import cz.cervenks.pia.application.data.entity.UserEntity;
import cz.cervenks.pia.application.data.entity.factory.LikeEntityFactory;
import cz.cervenks.pia.application.data.entity.factory.PostEntityFactory;
import cz.cervenks.pia.application.data.entity.factory.UserEntityFactory;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@AllArgsConstructor
@Component
public class LikeEntityFactoryImpl implements LikeEntityFactory {

    private final UserEntityFactory userEntityFactory;
    private final PostEntityFactory postEntityFactory;

    public LikeEntity makeLikeEntity(String userEmail, UUID postID) {
        UserEntity userEntity = userEntityFactory.getUserEntityFromDatabase(userEmail);
        PostEntity postEntity = postEntityFactory.getPostEntityFromDatabase(postID);

        LikeEntity likeEntity = new LikeEntity();
        likeEntity.setUser(userEntity);
        likeEntity.setPost(postEntity);
        return likeEntity;
    }
}

package cz.cervenks.pia.application.data.mapper.utils;

import cz.cervenks.pia.application.data.entity.LikeEntity;
import cz.cervenks.pia.application.data.entity.UserEntity;
import cz.cervenks.pia.application.data.mapper.Mapper;
import cz.cervenks.pia.business.domain.User;
import cz.cervenks.pia.business.usecases.feed.result.LikeState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class LikeInfo {
    @Getter
    private int count;

    @Getter
    private LikeState likeState;

    @Getter
    private List<User> users;

    private Mapper mapper;
    private String userEmail;


    //input is post and information about likes
    //output is 1) list of users liked
    //2) number of likes
    //3) if current users has liked the post
    public LikeInfo(Mapper mapper, String userEmail, Collection<LikeEntity> likeEntities, String postAuthorEmail) {
        this.mapper = mapper;
        this.userEmail = userEmail;
        this.users = new ArrayList<>();
        this.count = 0;
        this.likeState = LikeState.NOT_LIKED;

        for(LikeEntity likeEntity : likeEntities) {
            addUser(likeEntity);
            increaseCount();
            processLikedState(likeEntity);
        }

        processCantLikeState(userEmail, postAuthorEmail);
    }

    private void addUser(LikeEntity likeEntity) {
        UserEntity userEntity = likeEntity.getUser();
        User user = mapper.mapToUser(userEntity);
        this.users.add(user);
    }

    private void increaseCount() {
        this.count++;
    }

    private void processLikedState(LikeEntity likeEntity) {
        UserEntity userEntity = likeEntity.getUser();
        String likeEmail = userEntity.getEmail();

        if(likeEmail.equals(userEmail)) {
            this.likeState = LikeState.LIKED;
        }
    }

    private void processCantLikeState(String userEmail, String postAuthor) {
        if(userEmail.equals(postAuthor)) {
            this.likeState = LikeState.CANT_LIKE;
        }
    }
}

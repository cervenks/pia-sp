package cz.cervenks.pia.application.data.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import cz.cervenks.pia.application.data.entity.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, UUID> {

    Optional<UserEntity> findByEmail(String email);

    List<UserEntity> findByUsernameContains(String username);



}

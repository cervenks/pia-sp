package cz.cervenks.pia.application.data.model;

import cz.cervenks.pia.application.data.entity.MessageEntity;
import cz.cervenks.pia.application.data.entity.factory.MessageEntityFactory;
import cz.cervenks.pia.application.data.mapper.Mapper;
import cz.cervenks.pia.application.data.repository.MessageRepository;
import cz.cervenks.pia.business.domain.Message;
import cz.cervenks.pia.business.model.MessageModel;
import cz.cervenks.pia.business.usecases.chat.result.MessageInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component("messageModel")
@RequiredArgsConstructor
public class MessageModelImpl implements MessageModel {

    private final MessageEntityFactory messageEntityFactory;
    private final MessageRepository messageRepository;
    private final Mapper mapper;

    @Override
    public List<MessageInfo> getChatHistory(String invoker, String target, int limit) {
        List<MessageEntity> messageEntities = messageRepository.findMessages(invoker, target, limit);
        return mapMessages(invoker, messageEntities);
    }

    @Override
    public void createMessage(Message message) {
        MessageEntity messageEntity = messageEntityFactory.makeMessageEntity(message);
        messageRepository.save(messageEntity);
    }

    private List<MessageInfo> mapMessages(String invoker, List<MessageEntity> messageEntities) {
        //map entities to message info
        //in loop call the mapper method
        List<MessageInfo> result = new ArrayList<>();
        for(MessageEntity messageEntity : messageEntities) {
            MessageInfo messageInfo = mapper.mapMessageInfo(invoker, messageEntity);
            result.add(messageInfo);
        }
        return result;
    }
}

package cz.cervenks.pia.application.data.entity.factory;

import cz.cervenks.pia.application.data.entity.LikeEntity;

import java.util.UUID;

public interface LikeEntityFactory {

    LikeEntity makeLikeEntity(String userEmail, UUID postID);
}

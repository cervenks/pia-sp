package cz.cervenks.pia.application.data.entity.factory.impl;

import cz.cervenks.pia.application.data.entity.PostEntity;
import cz.cervenks.pia.application.data.entity.UserEntity;
import cz.cervenks.pia.application.data.entity.factory.PostEntityFactory;
import cz.cervenks.pia.application.data.entity.factory.UserEntityFactory;
import cz.cervenks.pia.application.data.repository.PostRepository;
import cz.cervenks.pia.business.domain.Post;
import cz.cervenks.pia.business.exception.InvalidDatabaseOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.Optional;
import java.util.UUID;

@Component
@RequiredArgsConstructor
public class PostEntityFactoryImpl implements PostEntityFactory {

    private final PostRepository postRepository;
    private final UserEntityFactory userEntityFactory;

    @Override
    public PostEntity makePostEntity(Post post) {
        String userEmail = post.getUserEmail();
        UserEntity userEntity = userEntityFactory.getUserEntityFromDatabase(userEmail);
        String text = post.getText();
        Timestamp timestamp = post.getTimestamp();
        boolean isAnnouncement = post.isAnnouncement();

        PostEntity postEntity = new PostEntity();
        postEntity.setUser(userEntity);
        postEntity.setText(text);
        postEntity.setTimestamp(timestamp);
        postEntity.setAnnouncement(isAnnouncement);

        return postEntity;
    }


    @Override
    @SneakyThrows
    public PostEntity getPostEntityFromDatabase(UUID uuid) {
        Optional<PostEntity> queryResult = postRepository.findById(uuid);
        if(queryResult.isEmpty()) {
            throw new InvalidDatabaseOperation("Post with id " + uuid.toString() + " not found.");
        } else {
            return queryResult.get();
        }
    }
}

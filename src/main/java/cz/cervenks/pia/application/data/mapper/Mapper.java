package cz.cervenks.pia.application.data.mapper;

import cz.cervenks.pia.application.data.entity.*;
import cz.cervenks.pia.application.data.mapper.utils.LikeInfo;
import cz.cervenks.pia.business.domain.RelationshipState;
import cz.cervenks.pia.business.domain.Role;
import cz.cervenks.pia.business.domain.User;
import cz.cervenks.pia.business.usecases.chat.result.MessageInfo;
import cz.cervenks.pia.business.usecases.feed.result.LikeState;
import cz.cervenks.pia.business.usecases.feed.result.PostInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.*;

@Component("mapper")
@Slf4j
public class Mapper {

    public User mapToUser(UserEntity entity) {
        String email = entity.getEmail();
        String username = entity.getUsername();
        String password = entity.getPassword();

        Set<RoleEntity> roleEntitySet = entity.getRoles();
        Set<Role> roles = mapToRoleList(roleEntitySet);

        return new User(username, email, password, roles);
    }

    private Set<Role> mapToRoleList(Iterable<RoleEntity> entities) {
        Set<Role> result = new HashSet<>();
        for(RoleEntity entity : entities) {
            tryToAddToRoleList(result, entity);
        }
        return result;
    }

    private void tryToAddToRoleList(Set<Role> set, RoleEntity entity) {
        try{
            Role role = mapToRole(entity);
            set.add(role);

        } catch (IllegalArgumentException e) {
            log.warn("Role from database called " + entity.getTitle() + "is not defined. Ignoring.");
        }
    }

    private Role mapToRole(RoleEntity entity) throws IllegalArgumentException {
        String title = entity.getTitle();
        return Role.valueOf(title);
    }

    public RelationshipState mapToStatus(RelationshipStateEntity entity) throws IllegalArgumentException {
        String title = entity.getTitle();
        return RelationshipState.valueOf(title);
    }

    public PostInfo mapToPostInfo(String userEmail, PostEntity postEntity, Collection<LikeEntity> likeEntities) {
        UUID id = postEntity.getId();
        String text = postEntity.getText();
        Timestamp timestamp = postEntity.getTimestamp();

        UserEntity userEntity = postEntity.getUser();
        String postAuthorEmail = userEntity.getEmail();
        User author = mapToUser(userEntity);

        LikeInfo likeInfo = new LikeInfo(this, userEmail, likeEntities, postAuthorEmail);

        List<User> usersLiked = likeInfo.getUsers();
        int likeCount = likeInfo.getCount();
        LikeState likeState = likeInfo.getLikeState();

        return new PostInfo(id, author, text, timestamp, likeCount, usersLiked, likeState);
    }

    public MessageInfo mapMessageInfo(String invoker, MessageEntity messageEntity) {
        Timestamp timestamp = messageEntity.getTimestamp();
        String text = messageEntity.getText();
        String sentBy = messageEntity.getSender().getEmail();
        boolean sentByInvoker = invoker.equals(sentBy);

        return new MessageInfo(sentByInvoker, timestamp, text);
    }

}

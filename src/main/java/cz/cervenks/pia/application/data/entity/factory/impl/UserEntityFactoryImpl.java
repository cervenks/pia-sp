package cz.cervenks.pia.application.data.entity.factory.impl;

import cz.cervenks.pia.application.data.entity.UserEntity;
import cz.cervenks.pia.application.data.entity.factory.UserEntityFactory;
import cz.cervenks.pia.application.data.repository.UserRepository;
import cz.cervenks.pia.business.domain.User;
import cz.cervenks.pia.business.exception.InvalidDatabaseOperation;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component("userEntityFactory")
@RequiredArgsConstructor
public class UserEntityFactoryImpl implements UserEntityFactory {

    private final PasswordEncoder encoder;
    private final UserRepository userRepository;

    @Override
    public UserEntity makeUserEntity(User user) {
        String email = user.getEmail();
        String username = user.getUsername();
        String password = user.getPassword();

        UserEntity userEntity = new UserEntity();
        userEntity.setEmail(email);
        userEntity.setUsername(username);
        userEntity.setPassword(encoder.encode(password));
        return userEntity;
    }

    @SneakyThrows
    @Override
    public UserEntity getUserEntityFromDatabase(String email) {
        Optional<UserEntity> queryResult = userRepository.findByEmail(email);
        if(queryResult.isEmpty()) {
            throw new InvalidDatabaseOperation("User email " + email + " not found.");
        } else {
            return queryResult.get();
        }
    }
}

package cz.cervenks.pia.application.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "relationship")
@Data
public class RelationshipEntity {

    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user1_id", nullable = false)
    private UserEntity user1;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user2_id", nullable = false)
    private UserEntity user2;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="state", nullable = false)
    private RelationshipStateEntity relationshipState;

    //throws stack overflow if default
    @Override
    public String toString() {
        return "Relationship: " + id.toString();
    }


}

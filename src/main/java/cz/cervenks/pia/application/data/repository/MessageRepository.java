package cz.cervenks.pia.application.data.repository;

import cz.cervenks.pia.application.data.entity.MessageEntity;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

public interface MessageRepository extends CrudRepository<MessageEntity, UUID> {

    @Query("SELECT me " +
            " FROM MessageEntity me " +
            " WHERE (me.sender.email = :userEmail1 AND me.receiver.email = :userEmail2) " +
            " OR (me.sender.email = :userEmail2 AND me.receiver.email = :userEmail1) " +
            " ORDER BY me.timestamp"
    )
    List<MessageEntity> findMessages(String userEmail1, String userEmail2, Pageable pageable);

    default List<MessageEntity> findMessages(String userEmail1, String userEmail2, int limit) {
        return findMessages(userEmail1, userEmail2, PageRequest.of(0, limit));
    }
}

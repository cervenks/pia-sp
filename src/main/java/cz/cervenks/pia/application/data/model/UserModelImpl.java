package cz.cervenks.pia.application.data.model;

import cz.cervenks.pia.application.data.entity.RoleEntity;
import cz.cervenks.pia.application.data.entity.UserEntity;
import cz.cervenks.pia.application.data.entity.factory.RoleEntityFactory;
import cz.cervenks.pia.application.data.entity.factory.UserEntityFactory;
import cz.cervenks.pia.application.data.mapper.Mapper;
import cz.cervenks.pia.application.data.repository.UserRepository;
import cz.cervenks.pia.business.domain.Role;
import cz.cervenks.pia.business.domain.User;
import cz.cervenks.pia.business.model.UserModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.*;

@RequiredArgsConstructor
@Component("userModel")
public class UserModelImpl implements UserModel {

    private final UserEntityFactory userEntityFactory;
    private final RoleEntityFactory roleEntityFactory;

    private final UserRepository userRepository;

    private final Mapper mapper;

    @Override
    public void addUser(User user) {
        UserEntity entity = userEntityFactory.makeUserEntity(user);
        Set<RoleEntity> userRoles = mapRoleEntities(user);
        entity.setRoles(userRoles);
        userRepository.save(entity);
    }

    @Override
    public User getIfExists(String email) {
        Optional<UserEntity> queryResult = userRepository.findByEmail(email);
        if(queryResult.isEmpty()) {
            return null;
        }

        UserEntity userEntity = queryResult.get();
        return mapper.mapToUser(userEntity);
    }

    private Set<RoleEntity> mapRoleEntities(User user) {
        HashSet<RoleEntity> roles = new HashSet<>();
        for(Role role : user.getRoles()) {
            RoleEntity entity = roleEntityFactory.getOrCreateNewEntity(role);
            roles.add(entity);
        }
        return roles;
    }

    @Override
    public List<User> getAllUsers(String userEmail, String filter) {
        List<UserEntity> userEntityList = userRepository.findByUsernameContains(filter);
        return createUserList(userEntityList);
    }

    private List<User> createUserList(Iterable<UserEntity> userEntityList) {
        List<User> result = new ArrayList<>();
        for(UserEntity entity : userEntityList) {
            User user = mapper.mapToUser(entity);
            result.add(user);
        }
        return result;
    }


}

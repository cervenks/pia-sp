package cz.cervenks.pia.application.data.entity.factory;


import cz.cervenks.pia.application.data.entity.UserEntity;
import cz.cervenks.pia.business.domain.User;

public interface UserEntityFactory {
    UserEntity makeUserEntity(User user);

    UserEntity getUserEntityFromDatabase(String email);
}

package cz.cervenks.pia.application.data.entity.factory;

import cz.cervenks.pia.application.data.entity.RelationshipEntity;
import cz.cervenks.pia.application.data.entity.UserEntity;
import cz.cervenks.pia.business.domain.RelationshipState;

public interface RelationshipFactory {
    RelationshipEntity getRelationshipEntityFromDatabase(String userEmail1, String userEmail2);
    RelationshipEntity makeRelationshipEntity(UserEntity userEntity1, UserEntity userEntity2, RelationshipState status);
    RelationshipEntity makeRelationshipEntity(String userEmail1, String userEmail2, RelationshipState state);
}

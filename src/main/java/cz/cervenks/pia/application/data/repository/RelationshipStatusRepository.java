package cz.cervenks.pia.application.data.repository;

import cz.cervenks.pia.application.data.entity.RelationshipStateEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface RelationshipStatusRepository extends CrudRepository<RelationshipStateEntity, UUID> {

    Optional<RelationshipStateEntity> findByTitle(String title);
}

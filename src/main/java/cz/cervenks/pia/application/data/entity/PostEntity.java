package cz.cervenks.pia.application.data.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "post")
@Data
public class PostEntity {
    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="author_id", nullable = false)
    private UserEntity user;

    @Column(nullable = false)
    private String text;

    @Column(nullable = false)
    private Timestamp timestamp;

    @Column(name = "is_announcement", nullable = false)
    boolean isAnnouncement;

    //throws stack overflow if default
    @Override
    public String toString() {
        return "Post: " + id.toString();
    }

}

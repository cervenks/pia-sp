package cz.cervenks.pia.application.data.entity.factory.impl;

import cz.cervenks.pia.application.data.entity.RelationshipStateEntity;
import cz.cervenks.pia.application.data.entity.factory.RelationshipStateFactory;
import cz.cervenks.pia.application.data.repository.RelationshipStatusRepository;
import cz.cervenks.pia.business.domain.RelationshipState;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@AllArgsConstructor
@Component
public class RelationshipStateFactoryImpl implements RelationshipStateFactory {

    private final RelationshipStatusRepository relationshipStatusRepository;

    @Override
    public RelationshipStateEntity getOrCreateNewEntity(RelationshipState relationShipState) {
        String title = relationShipState.name();
        Optional<RelationshipStateEntity> queryResult = relationshipStatusRepository.findByTitle(title);

        //if the entity is not present in query result,
        //we will call method create new entity a return result
        return queryResult.orElseGet(() -> createNewEntity(title));
    }

    private RelationshipStateEntity createNewEntity(String title) {
        RelationshipStateEntity relationshipStateEntity = new RelationshipStateEntity();
        relationshipStateEntity.setTitle(title);

        relationshipStatusRepository.save(relationshipStateEntity);
        return relationshipStateEntity;
    }
}

package cz.cervenks.pia.application.data.entity.factory;

import cz.cervenks.pia.application.data.entity.PostEntity;
import cz.cervenks.pia.business.domain.Post;

import java.util.UUID;

public interface PostEntityFactory {

    PostEntity makePostEntity(Post post);
    PostEntity getPostEntityFromDatabase(UUID uuid);
}

package cz.cervenks.pia.application.presentation.controllers.impl;

import cz.cervenks.pia.application.presentation.controllers.api.SearchController;
import cz.cervenks.pia.application.presentation.service.api.RelationshipService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/relationship")
@RequiredArgsConstructor
public class SearchControllerImpl implements SearchController {

    private final RelationshipService relationshipService;

    @Override
    @PostMapping("/search")
    public void search(@RequestBody String value)
    {
        relationshipService.search(value);
    }

}

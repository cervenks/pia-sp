package cz.cervenks.pia.application.presentation.utils.role_action;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.admin.api.RoleManagement;
import cz.cervenks.pia.business.usecases.admin.result.AdminAssignResult;

public class RevokeAdminButton extends RoleButton {
    public RevokeAdminButton(RoleManagement roleManagement, LoggedUserService loggedUserService, RoleActionLine parentLine) {
        super(roleManagement, loggedUserService, parentLine);
    }

    @Override
    public String getButtonText() {
        return "Odebrat admina";
    }

    @Override
    public String getButtonClass() {
        return "btn-danger";
    }

    protected AdminAssignResult proceedRequest(String loggedUser, String targetUser) {
        return roleManagement.removeAdmin(loggedUser, targetUser);
    }

    protected String getSuccessMessage() {
        return "Uživatel úspěšně odebrán admin.";
    }

}

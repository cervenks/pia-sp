package cz.cervenks.pia.application.presentation.utils.user_action;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.relationship.api.ActionType;
import cz.cervenks.pia.business.usecases.relationship.api.RelationshipManagement;

public class RejectButton extends UserActionButton {

    public RejectButton(RelationshipManagement relationshipManagement, LoggedUserService loggedUserService, UserActionLine parentLine) {
        super(relationshipManagement, loggedUserService, parentLine);
    }


    @Override
    protected ActionType getActionType() {
        return ActionType.REJECT;
    }

    @Override
    protected String getSuccessMessage() {
        return "Žádost o přátelství odmítnuta.";
    }

    @Override
    public String getButtonText() {
        return "Odmítnout";
    }

    @Override
    public String getButtonClass() {
        return "btn-danger";
    }
}

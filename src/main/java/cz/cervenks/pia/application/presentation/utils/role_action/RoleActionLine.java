package cz.cervenks.pia.application.presentation.utils.role_action;

import cz.cervenks.pia.application.presentation.utils.user_action.ButtonFactory;
import cz.cervenks.pia.application.presentation.utils.user_action.ButtonType;
import cz.cervenks.pia.application.presentation.utils.user_action.UserActionButton;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;


public class RoleActionLine {

    private final RoleButtonFactory roleButtonFactory;

    @Getter
    private String username;

    @Getter
    private String email;

    @Getter
    private boolean hasText = false;

    @Getter
    private String text = "";

    @Getter
    private List<RoleButton> buttons = new ArrayList<>();

    public RoleActionLine(RoleButtonFactory roleButtonFactory, String username, String email) {
        this.roleButtonFactory = roleButtonFactory;
        this.username = username;
        this.email = email;
    }

    public void addGrantAdminButton() {
        RoleButton roleButton = roleButtonFactory.makeGrantAdminButton(this);
        this.buttons.add(roleButton);
    }

    public void addRevokeAdminButton() {
        RoleButton roleButton = roleButtonFactory.makeRevokeAdminButton(this);
        this.buttons.add(roleButton);
    }

    public void displayResult(String text) {
        this.text = text;
        this.hasText = true;
    }

}

package cz.cervenks.pia.application.presentation.utils.feed;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.feed.api.PostManagement;
import cz.cervenks.pia.business.usecases.feed.result.LikeState;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PostButtonFactory {

    private final PostManagement postManagement;
    private final LoggedUserService loggedUserService;

    public PostButton makeButton(PostView postView) {
        LikeState likeState = postView.getLikeState();
        switch (likeState) {
            case LIKED:
                return new UnlikeButton(postManagement, loggedUserService, postView);
            case NOT_LIKED:
                return new LikeButton(postManagement, loggedUserService, postView);
            case CANT_LIKE:
            default:
                return new NoButton(postManagement, loggedUserService, postView);
        }
    }
}

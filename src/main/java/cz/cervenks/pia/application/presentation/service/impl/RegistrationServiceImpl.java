package cz.cervenks.pia.application.presentation.service.impl;

import cz.cervenks.pia.application.presentation.auth.OnlineUserStore;
import cz.cervenks.pia.application.presentation.service.api.RegistrationService;
import cz.cervenks.pia.business.usecases.registration.api.Registration;
import cz.cervenks.pia.business.usecases.registration.dependency.UserToRegister;
import cz.cervenks.pia.business.usecases.registration.impl.RegistrationResult;
import cz.cervenks.pia.business.usecases.registration.impl.RegistrationResultStatus;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@Service(value = "registrationService")
@RequiredArgsConstructor
@ViewScoped
public class RegistrationServiceImpl implements RegistrationService, UserToRegister {

    private final Registration registration;
    private final OnlineUserStore onlineUserStore;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private String passwordCheck;

    @Getter
    private List<String> alertBoxLines = new ArrayList<>();


    @SneakyThrows
    @Override
    public void register() {
        if(!performRegistering()) {
            return;
        }

        //login();
        FacesContext.getCurrentInstance().getExternalContext().redirect("/register_success");
    }

    private boolean performRegistering() {
        RegistrationResult result = registration.register(this);
        RegistrationResultStatus status = result.getRegistrationResultStatus();

        if(status == RegistrationResultStatus.VALIDATION_FAILED) {
            alertBoxLines = result.getErrors();
            return false;
        }

        return true;
    }

    private void login() {
        Authentication authentication = new UsernamePasswordAuthenticationToken(email, password);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        onlineUserStore.addUser(email);
    }
}
package cz.cervenks.pia.application.presentation.controllers.api;

import cz.cervenks.pia.application.presentation.utils.PasswordPair;
import cz.cervenks.pia.business.usecases.validation.implementation.ValidationResult;

public interface RegistrationController {

    ValidationResult validateEmail(String value);
    ValidationResult validateUsername(String value);
    ValidationResult validatePassword(PasswordPair passwordPair);
}

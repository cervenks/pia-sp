package cz.cervenks.pia.application.presentation.service.impl;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service("loggedUserService")
public class LoggedUserServiceImpl implements LoggedUserService {

    @Override
    public UserDetails getUser() {
        final var authentication = SecurityContextHolder.getContext().getAuthentication();

        if(authentication.getPrincipal().equals("anonymousUser")) {
            return null;
        }

        if (authentication != null) {
            return (UserDetails)authentication.getPrincipal();
        }

        return null;
    }
}

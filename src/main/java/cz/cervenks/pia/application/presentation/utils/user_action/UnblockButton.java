package cz.cervenks.pia.application.presentation.utils.user_action;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.relationship.api.ActionType;
import cz.cervenks.pia.business.usecases.relationship.api.RelationshipManagement;

public class UnblockButton extends UserActionButton {

    public UnblockButton(RelationshipManagement relationshipManagement, LoggedUserService loggedUserService, UserActionLine parentLine) {
        super(relationshipManagement, loggedUserService, parentLine);
    }


    @Override
    protected ActionType getActionType() {
        return ActionType.UNBLOCK;
    }

    @Override
    protected String getSuccessMessage() {
        return "Uživatel úspěšně odblokován.";
    }

    @Override
    public String getButtonText() {
        return "Odblokovat";
    }

    @Override
    public String getButtonClass() {
        return "btn-light";
    }
}

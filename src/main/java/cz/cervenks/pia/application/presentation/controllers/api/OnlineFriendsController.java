package cz.cervenks.pia.application.presentation.controllers.api;

import java.util.Collection;

public interface OnlineFriendsController {

    Collection<String> getOnlineFriends();
}

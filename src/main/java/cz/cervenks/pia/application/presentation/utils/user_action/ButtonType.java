package cz.cervenks.pia.application.presentation.utils.user_action;

public enum  ButtonType {
    ACCEPT, UNFRIEND, BLOCK, REJECT, REQUEST, REVOKE, UNBLOCK
}

package cz.cervenks.pia.application.presentation.service.api;

import cz.cervenks.pia.application.presentation.utils.feed.PostView;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public interface DashboardService {
    void showMore();

    void refresh();

    void addPost();
}

package cz.cervenks.pia.application.presentation.utils.user_action;

import cz.cervenks.pia.business.domain.RelationshipState;
import cz.cervenks.pia.business.domain.User;
import cz.cervenks.pia.business.usecases.relationship.result.UserWithState;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserActionLineFactory {

    private final ButtonFactory buttonFactory;


    public UserActionLine makeLine(UserWithState userWithState) {
        RelationshipState state = userWithState.getState();
        User user = userWithState.getUser();

        switch(state) {
            case NO_RELATIONSHIP:
                return makeNoRelationshipLine(user);
            case REQUESTED_BY_ME:
                return makeRequestedByMeLine(user);
            case REQUESTED_BY_HIM:
                return makeRequestedByHimLine(user);
            case FRIENDS:
                return makeFriendsLine(user);
            case BLOCKED_BY_ME:
                return makeBlockedByMeLine(user);
            case BLOCKED_BY_HIM:
            default:
                return null;
        }
    }

    private UserActionLine makeNoRelationshipLine(User user) {
        UserActionLine line = makeEmptyLine(user);
        line.addButton(ButtonType.REQUEST);
        line.addButton(ButtonType.BLOCK);
        return line;
    }

    private UserActionLine makeRequestedByMeLine(User user) {
        UserActionLine line = makeEmptyLine(user);
        line.addButton(ButtonType.REVOKE);
        line.addButton(ButtonType.BLOCK);
        return line;
    }

    private UserActionLine makeRequestedByHimLine(User user) {
        UserActionLine line = makeEmptyLine(user);
        line.addButton(ButtonType.ACCEPT);
        line.addButton(ButtonType.REJECT);
        line.addButton(ButtonType.BLOCK);
        return line;
    }

    private UserActionLine makeFriendLine(User user) {
        UserActionLine line = makeEmptyLine(user);
        line.addButton(ButtonType.UNFRIEND);
        line.addButton(ButtonType.BLOCK);
        return line;
    }

    private UserActionLine makeBlockedByMeLine(User user) {
        UserActionLine line = makeEmptyLine(user);
        line.addButton(ButtonType.REQUEST);
        line.addButton(ButtonType.UNBLOCK);
        return line;
    }

    private UserActionLine makeFriendsLine(User user) {
        UserActionLine line = makeEmptyLine(user);
        line.addButton(ButtonType.UNFRIEND);
        line.addButton(ButtonType.BLOCK);
        return line;
    }


    private UserActionLine makeEmptyLine(User user) {
        String username = user.getUsername();
        String email = user.getEmail();
        return new UserActionLine(buttonFactory, username, email);
    }
}

package cz.cervenks.pia.application.presentation.controllers.api;

public interface SearchController {
    void search(String value);
}

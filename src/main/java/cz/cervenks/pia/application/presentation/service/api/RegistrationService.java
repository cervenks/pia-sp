package cz.cervenks.pia.application.presentation.service.api;

public interface RegistrationService {

    void register();

}

package cz.cervenks.pia.application.presentation.utils.user_action;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.relationship.api.RelationshipManagement;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class ButtonFactory {

   private final RelationshipManagement relationshipManagement;
   private final LoggedUserService loggedUserService;

    public UserActionButton makeButton(UserActionLine line, ButtonType type) {
        switch(type) {
            case ACCEPT:
                return new AcceptButton(relationshipManagement, loggedUserService, line);
            case UNFRIEND:
                return new UnfriendButton(relationshipManagement, loggedUserService, line);
            case BLOCK:
                return new BlockButton(relationshipManagement, loggedUserService, line);
            case REJECT:
                return new RejectButton(relationshipManagement, loggedUserService, line);
            case REQUEST:
                return new RequestButton(relationshipManagement, loggedUserService, line);
            case REVOKE:
                return new RevokeButton(relationshipManagement, loggedUserService, line);
            case UNBLOCK:
                return new UnblockButton(relationshipManagement, loggedUserService, line);
        }
        return null;
    }
}

package cz.cervenks.pia.application.presentation.service.impl;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.application.presentation.service.api.RelationshipService;
import cz.cervenks.pia.application.presentation.utils.user_action.UserActionLine;
import cz.cervenks.pia.application.presentation.utils.user_action.UserActionLineFactory;
import cz.cervenks.pia.business.usecases.relationship.api.UserSearch;
import cz.cervenks.pia.business.usecases.relationship.result.UserWithState;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.SessionScope;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service("relationshipService")
@RequiredArgsConstructor
@Slf4j
//must handle search controller requests
@SessionScope
public class RelationshipServiceImpl implements RelationshipService {

    private final LoggedUserService loggedUserService;
    private final UserSearch userSearch;
    private final UserActionLineFactory userActionLineFactory;

    @Getter
    @Setter
    private String searchBar;

    @Getter
    private List<UserActionLine> searchResult;

    @Getter
    private List<UserActionLine> pendingRequests;

    @Getter
    private List<UserActionLine> blockedUsers;

    @Getter
    private List<UserActionLine> friends;

    @PostConstruct
    private void construct() {
        String email = loggedUserService.getUser().getUsername();
        constructPending(email);
        constructBlocked(email);
        constructFriends(email);
    }

    @Override
    public void search(String text) {
        this.searchBar = text;
        search();
    }

    @Override
    public void search() {
        String email = loggedUserService.getUser().getUsername();
        constructPending(email);
        constructBlocked(email);
        constructFriends(email);
        constructSearch(email);
    }



    private void constructPending(String email) {
        List<UserWithState> users = userSearch.listPending(email);
        pendingRequests = convert(users);
    }

    private void constructBlocked(String email) {
        List<UserWithState> users = userSearch.listBlocked(email);
        blockedUsers = convert(users);
    }

    private void constructFriends(String email) {
        List<UserWithState> users = userSearch.listFriends(email);
        friends = convert(users);
    }

    private void constructSearch(String email) {
        List<UserWithState> users = userSearch.search(email, searchBar);
        searchResult = convert(users);
    }

    private List<UserActionLine> convert(List<UserWithState> users) {
        List<UserActionLine> result = new ArrayList<>();
        for(UserWithState userWithState : users) {
            UserActionLine line = userActionLineFactory.makeLine(userWithState);
            if(line != null) {
                result.add(line);
            }
        }
        return result;
    }
}

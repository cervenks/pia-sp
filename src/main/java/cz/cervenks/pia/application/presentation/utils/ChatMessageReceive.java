package cz.cervenks.pia.application.presentation.utils;

import cz.cervenks.pia.business.usecases.chat.result.MessageInfo;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Locale;

@Data
@AllArgsConstructor
public class ChatMessageReceive {
    private String text;
    private String otherUserEmail;
    private String timestamp;
    private boolean sentByMe;


    public ChatMessageReceive(String text, String otherUserEmail, Timestamp timestamp, boolean sentByMe) {
        this.text = text;
        this.otherUserEmail = otherUserEmail;
        this.timestamp = getFormattedTimestamp(timestamp);
        this.sentByMe = sentByMe;
    }

    public static ChatMessageReceive fromMessageInfo(MessageInfo messageInfo, String otherUser) {
        String text = messageInfo.getText();
        boolean sentByMe = messageInfo.isSentByInvoker();
        Timestamp timestamp = messageInfo.getTimestamp();
        String timestampText = getFormattedTimestamp(timestamp);
        return new ChatMessageReceive(text, otherUser, timestampText, sentByMe);
    }

    private static String getFormattedTimestamp(Timestamp timestamp) {
        String pattern = "dd.MM.yyyy, HH:mm";
        Locale locale = new Locale("cs", "CZ");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, locale);
        return simpleDateFormat.format(timestamp);
    }
}

package cz.cervenks.pia.application.presentation.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

@AllArgsConstructor
public class LoggedUser implements HttpSessionBindingListener {

    private String email;
    private OnlineUserStore onlineUserStore;

    @Override
    public void valueBound(HttpSessionBindingEvent event) {
        LoggedUser user = (LoggedUser) event.getValue();
        String email = user.email;
        onlineUserStore.addUser(email);
    }

    @Override
    public void valueUnbound(HttpSessionBindingEvent event) {
        LoggedUser user = (LoggedUser) event.getValue();
        String email = user.email;
        onlineUserStore.removeUser(email);
    }
}

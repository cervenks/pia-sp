package cz.cervenks.pia.application.presentation.auth;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FriendsUpdateMessage {

    private String userEmail;
    private String username;
    private AuthType authType;

}

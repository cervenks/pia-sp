package cz.cervenks.pia.application.presentation.auth;

public enum AuthType {
    LOGIN, LOGOUT
}

package cz.cervenks.pia.application.presentation.utils.feed;

import cz.cervenks.pia.business.usecases.feed.result.PostInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PostViewFactory {
    private final PostButtonFactory postButtonFactory;

    public PostView makePostView(PostInfo postInfo) {
        return new PostView(postInfo, postButtonFactory);
    }
}

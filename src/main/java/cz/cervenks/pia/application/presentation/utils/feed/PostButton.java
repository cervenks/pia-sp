package cz.cervenks.pia.application.presentation.utils.feed;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.feed.api.PostManagement;
import cz.cervenks.pia.business.usecases.feed.result.PostInfo;
import lombok.AllArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
public abstract class PostButton {
    private final PostManagement postManagement;
    private final LoggedUserService loggedUserService;
    private PostView parent;

    public abstract String getButtonText();
    public abstract String getButtonClass();
    public abstract boolean showButton();

    public void clickButton() {
        String userEmail = loggedUserService.getUser().getUsername();
        UUID postId = parent.getIdentification();
        PostInfo postInfo = postManagement.toggleLike(postId, userEmail);
        this.parent.refreshLikesFrom(postInfo);

    }
}

package cz.cervenks.pia.application.presentation.utils.user_action;


import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.relationship.api.ActionType;
import cz.cervenks.pia.business.usecases.relationship.api.RelationshipManagement;

public class UnfriendButton extends UserActionButton {

    public UnfriendButton(RelationshipManagement relationshipManagement, LoggedUserService loggedUserService, UserActionLine parentLine) {
        super(relationshipManagement, loggedUserService, parentLine);
    }

    @Override
    protected ActionType getActionType() {
        return ActionType.UNFRIEND;
    }

    @Override
    protected String getSuccessMessage() {
        return "Uživatel odebrán z přátel.";
    }

    @Override
    public String getButtonText() {
        return "Odebrat z přátel";
    }

    @Override
    public String getButtonClass() {
        return "btn-danger";
    }
}

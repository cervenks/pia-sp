package cz.cervenks.pia.application.presentation.service.impl;

import cz.cervenks.pia.application.presentation.service.api.DashboardService;
import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.application.presentation.utils.feed.PostView;
import cz.cervenks.pia.application.presentation.utils.feed.PostViewFactory;
import cz.cervenks.pia.business.usecases.admin.api.AnnouncementManagement;
import cz.cervenks.pia.business.usecases.feed.api.PostManagement;
import cz.cervenks.pia.business.usecases.feed.result.PostInfo;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service(value = "dashboardService")
@RequiredArgsConstructor
@ViewScoped
public class DashboardServiceImpl implements DashboardService {

    private final PostViewFactory postViewFactory;
    private final LoggedUserService loggedUserService;
    private final PostManagement postManagement;
    private final AnnouncementManagement announcementManagement;

    @Getter
    @Setter
    private String newPostText;

    @Getter
    @Setter
    private boolean newPostAnnouncement;


    @Getter
    private List<PostView> posts;

    private int limit;
    private Timestamp timestamp;

    @Override
    public void showMore() {
        limit += 5;
        setupPosts();
    }

    @Override
    public void refresh() {
        setupPosts();
    }

    @Override
    public void addPost() {
        String email = loggedUserService.getUser().getUsername();
        if(newPostAnnouncement) {
            announcementManagement.newAnnouncement(email, newPostText);
        } else {
            postManagement.newPost(email, newPostText);
        }

        setupPosts(email);
    }

    @PostConstruct
    private void construct() {
        this.limit = 5;
        this.timestamp = Timestamp.from(Instant.now());

        setupPosts();
    }

    private void setupPosts() {
        String email = loggedUserService.getUser().getUsername();
        setupPosts(email);
    }

    private void setupPosts(String email) {
        List<PostInfo> postInfos = postManagement.showPosts(limit, email, timestamp);
        setupPosts(postInfos);
    }


    private void setupPosts(List<PostInfo> postInfos) {
        posts = new ArrayList<>();
        for(PostInfo postInfo : postInfos) {
            PostView postView = postViewFactory.makePostView(postInfo);
            posts.add(postView);
        }
    }

}

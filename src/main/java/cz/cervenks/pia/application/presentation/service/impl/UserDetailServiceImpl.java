package cz.cervenks.pia.application.presentation.service.impl;

import cz.cervenks.pia.application.data.entity.RoleEntity;
import cz.cervenks.pia.application.data.entity.UserEntity;
import cz.cervenks.pia.application.data.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service("userDetailsService")
@RequiredArgsConstructor
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<UserEntity> queryResult = userRepository.findByEmail(email);
        if (queryResult.isEmpty()) {
            throw new UsernameNotFoundException(email + " not found.");
        }

        UserEntity user = queryResult.get();
        return new org.springframework.security.core.userdetails.User(
                user.getEmail(), user.getPassword(), true, true, true,
                true, getAuthorities(user.getRoles()));
    }

    private Collection<? extends GrantedAuthority> getAuthorities(
            Collection<RoleEntity> roles) {

        List<GrantedAuthority> result = new ArrayList<>();
        for(RoleEntity role : roles) {
            result.add(new SimpleGrantedAuthority("ROLE_" + role.getTitle()));
        }
        return result;
    }
}

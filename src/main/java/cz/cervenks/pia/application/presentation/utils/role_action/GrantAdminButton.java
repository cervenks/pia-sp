package cz.cervenks.pia.application.presentation.utils.role_action;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.admin.api.RoleManagement;
import cz.cervenks.pia.business.usecases.admin.result.AdminAssignResult;

public class GrantAdminButton extends RoleButton {
    public GrantAdminButton(RoleManagement roleManagement, LoggedUserService loggedUserService, RoleActionLine parentLine) {
        super(roleManagement, loggedUserService, parentLine);
    }

    @Override
    public String getButtonText() {
        return "Nastavit admina";
    }

    @Override
    public String getButtonClass() {
        return "btn-success";
    }

    @Override
    protected AdminAssignResult proceedRequest(String loggedUser, String targetEmail) {
        return roleManagement.assignAdmin(loggedUser, targetEmail);
    }

    @Override
    protected String getSuccessMessage() {
        return "Uživatel úspěšně změnen na admina.";
    }

}

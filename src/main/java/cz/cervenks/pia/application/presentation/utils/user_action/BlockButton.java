package cz.cervenks.pia.application.presentation.utils.user_action;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.relationship.api.ActionType;
import cz.cervenks.pia.business.usecases.relationship.api.RelationshipManagement;

public class BlockButton extends UserActionButton {

    public BlockButton(RelationshipManagement relationshipManagement, LoggedUserService loggedUserService, UserActionLine parentLine) {
        super(relationshipManagement, loggedUserService, parentLine);
    }


    @Override
    protected ActionType getActionType() {
        return ActionType.BLOCK;
    }

    @Override
    protected String getSuccessMessage() {
        return "Uživatel úspěšně zablokován.";
    }

    @Override
    public String getButtonText() {
        return "Zablokovat";
    }

    @Override
    public String getButtonClass() {
        return "btn-dark";
    }
}

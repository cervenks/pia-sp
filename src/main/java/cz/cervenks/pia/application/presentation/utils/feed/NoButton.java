package cz.cervenks.pia.application.presentation.utils.feed;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.feed.api.PostManagement;

public class NoButton extends PostButton {


    public NoButton(PostManagement postManagement, LoggedUserService loggedUserService, PostView parent) {
        super(postManagement, loggedUserService, parent);
    }

    @Override
    public String getButtonText() {
        return null;
    }

    @Override
    public String getButtonClass() {
        return null;
    }

    @Override
    public boolean showButton() {
        return false;
    }
}

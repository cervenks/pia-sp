package cz.cervenks.pia.application.presentation.utils.user_action;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.relationship.api.ActionType;
import cz.cervenks.pia.business.usecases.relationship.api.RelationshipManagement;

public class RequestButton extends UserActionButton {


    public RequestButton(RelationshipManagement relationshipManagement, LoggedUserService loggedUserService, UserActionLine parentLine) {
        super(relationshipManagement, loggedUserService, parentLine);
    }

    @Override
    protected ActionType getActionType() {
        return ActionType.REQUEST;
    }

    @Override
    protected String getSuccessMessage() {
        return "Uživatel požádán o přátelství.";
    }

    @Override
    public String getButtonText() {
        return "Požádat o přátelství";
    }

    @Override
    public String getButtonClass() {
        return "btn-primary";
    }
}

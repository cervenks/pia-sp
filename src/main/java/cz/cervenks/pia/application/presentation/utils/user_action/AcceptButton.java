package cz.cervenks.pia.application.presentation.utils.user_action;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.relationship.api.ActionType;
import cz.cervenks.pia.business.usecases.relationship.api.RelationshipManagement;

public class AcceptButton extends UserActionButton {

    public AcceptButton(RelationshipManagement relationshipManagement, LoggedUserService loggedUserService, UserActionLine parentLine) {
        super(relationshipManagement, loggedUserService, parentLine);
    }


    @Override
    public String getButtonText() {
        return "Přijmout";
    }

    @Override
    public String getButtonClass() {
        return "btn-success";
    }

    @Override
    protected ActionType getActionType() {
        return ActionType.ACCEPT;
    }

    @Override
    protected String getSuccessMessage() {
        return "Žádost o přátelství přijata.";
    }
}

package cz.cervenks.pia.application.presentation.utils.role_action;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.admin.api.RoleManagement;
import cz.cervenks.pia.business.usecases.admin.result.AdminAssignResult;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class RoleButton {

    protected final RoleManagement roleManagement;
    protected final LoggedUserService loggedUserService;
    protected final RoleActionLine parentLine;

    public abstract String getButtonText();
    public abstract String getButtonClass();
    protected abstract AdminAssignResult proceedRequest(String loggedUser, String targetEmail);
    protected abstract String getSuccessMessage();

    public void doAction() {
        String loggedEmail = loggedUserService.getUser().getUsername();
        String targetEmail = parentLine.getEmail();
        AdminAssignResult result = proceedRequest(loggedEmail, targetEmail);

        if(result != AdminAssignResult.OK) {
            displayErrorResult(result);
        } else {
            String message = getSuccessMessage();
            parentLine.displayResult(message);
        }
    }


    private void displayErrorResult(AdminAssignResult adminAssignResult) {
        String message = getErrorResult(adminAssignResult);
        parentLine.displayResult(message);
    }

    private String getErrorResult(AdminAssignResult adminAssignResult) {
        switch(adminAssignResult) {
            case INVOKER_NOT_ADMIN:
                return "Nemáte práva admina.";
            case INVOKER_SAME_AS_TARGET:
                return "Nemůžete změnit práva admina sobě.";
            case ALREADY_IS:
                return "Uživatel už má požadované role.";
            case OK:
            default:
                return "Neznámá chyba.";
        }
    }



}

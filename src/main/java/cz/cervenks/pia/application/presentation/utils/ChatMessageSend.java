package cz.cervenks.pia.application.presentation.utils;

import lombok.Data;

@Data
public class ChatMessageSend {
    private String text;
    private String whom;
}

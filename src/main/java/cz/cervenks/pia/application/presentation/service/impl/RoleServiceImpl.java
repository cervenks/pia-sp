package cz.cervenks.pia.application.presentation.service.impl;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.application.presentation.service.api.RoleService;
import cz.cervenks.pia.application.presentation.utils.role_action.RoleActionLine;
import cz.cervenks.pia.application.presentation.utils.role_action.RoleActionLineFactory;
import cz.cervenks.pia.business.domain.Role;
import cz.cervenks.pia.business.domain.User;
import cz.cervenks.pia.business.usecases.relationship.api.UserSearch;
import cz.cervenks.pia.business.usecases.relationship.result.UserWithState;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@ViewScoped
public class RoleServiceImpl implements RoleService {

    private final LoggedUserService loggedUserService;
    private final UserSearch userSearch;
    private final RoleActionLineFactory roleActionLineFactory;

    @Getter
    private List<RoleActionLine> lines;


    @PostConstruct
    private void construct() {
        String email = loggedUserService.getUser().getUsername();
        List<UserWithState> result = userSearch.listFriends(email);
        constructUsers(result);
    }

    private void constructUsers(List<UserWithState> usersWithState) {
        lines = new ArrayList<>();
        for(UserWithState userWithState : usersWithState) {
            User user = userWithState.getUser();
            boolean isAdmin = user.hasRole(Role.ADMIN);
            String username = user.getUsername();
            String email = user.getEmail();

            RoleActionLine roleActionLine;
            if(isAdmin) {
                roleActionLine = roleActionLineFactory.makeRevokeAdminLine(username, email);
            } else {
                roleActionLine = roleActionLineFactory.makeGrantAdminLine(username, email);
            }
            lines.add(roleActionLine);
        }
    }

}

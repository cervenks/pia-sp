package cz.cervenks.pia.application.presentation.controllers.api;

import cz.cervenks.pia.application.presentation.utils.ChatMessageReceive;
import cz.cervenks.pia.application.presentation.utils.ChatMessageSend;
import cz.cervenks.pia.business.usecases.chat.result.MessageInfo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface ChatController {

    void sendMessage(ChatMessageSend chatMessageSend);
    List<ChatMessageReceive> getChatHistory(String otherUser);
}

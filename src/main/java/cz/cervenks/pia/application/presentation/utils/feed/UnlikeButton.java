package cz.cervenks.pia.application.presentation.utils.feed;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.feed.api.PostManagement;

public class UnlikeButton extends PostButton {


    public UnlikeButton(PostManagement postManagement, LoggedUserService loggedUserService, PostView parent) {
        super(postManagement, loggedUserService, parent);
    }

    @Override
    public String getButtonText() {
        return "Příspěvek už se mi nelíbí";
    }

    @Override
    public String getButtonClass() {
        return "btn-warning";
    }

    @Override
    public boolean showButton() {
        return true;
    }
}

package cz.cervenks.pia.application.presentation.utils.role_action;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.admin.api.RoleManagement;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RoleButtonFactory {

    private final RoleManagement roleManagement;
    private final LoggedUserService loggedUserService;

    public RoleButton makeGrantAdminButton(RoleActionLine roleActionLine) {
        return new GrantAdminButton(roleManagement, loggedUserService, roleActionLine);
    }

    public RoleButton makeRevokeAdminButton(RoleActionLine roleActionLine) {
        return new RevokeAdminButton(roleManagement, loggedUserService, roleActionLine);
    }
}

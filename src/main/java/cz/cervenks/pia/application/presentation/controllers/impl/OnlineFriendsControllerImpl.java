package cz.cervenks.pia.application.presentation.controllers.impl;


import cz.cervenks.pia.application.presentation.auth.OnlineUserStore;
import cz.cervenks.pia.application.presentation.controllers.api.OnlineFriendsController;
import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/api/")
@RequiredArgsConstructor
public class OnlineFriendsControllerImpl implements OnlineFriendsController {

    private final LoggedUserService loggedUserService;
    private final OnlineUserStore onlineUserStore;

    @Override
    @GetMapping("/online_friends")
    public Collection<String> getOnlineFriends() {
        String userEmail = loggedUserService.getUser().getUsername();
        return onlineUserStore.readUser(userEmail);

    }


}

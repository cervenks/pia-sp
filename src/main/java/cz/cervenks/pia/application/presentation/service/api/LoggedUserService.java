package cz.cervenks.pia.application.presentation.service.api;

import org.springframework.security.core.userdetails.UserDetails;

public interface LoggedUserService {
    UserDetails getUser();
}

package cz.cervenks.pia.application.presentation.websockets;


import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;

import java.security.Principal;
import java.util.Arrays;

@Slf4j
public class TopicSubscriptionInterceptor implements ChannelInterceptor {
    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        StompHeaderAccessor headerAccessor= StompHeaderAccessor.wrap(message);
        if (StompCommand.SUBSCRIBE.equals(headerAccessor.getCommand())) {
            Principal userPrincipal = headerAccessor.getUser();
            if(!validateSubscription(userPrincipal, headerAccessor.getDestination()))
            {
                throw new IllegalArgumentException("No permission for this topic.");
            }
        }
        return message;
    }

    private boolean validateSubscription(Principal userPrincipal, String path) {
        String userEmail = userPrincipal.getName();
        String[] tokens = path.split("/");
        if(tokens[0].equals("")) {
            tokens = removeFirstElement(tokens);
        }

        //if the path is in /user/{userEmail}/** format
        //the user email must be equal to currently logged user
        if(tokens.length < 2) {
            return true;
        }

        if(!tokens[0].equals("user")) {
            return true;
        }

        return tokens[1].equals(userEmail);
    }

    private String[] removeFirstElement(String[] array) {
        return Arrays.copyOfRange(array, 1, array.length);
    }
}

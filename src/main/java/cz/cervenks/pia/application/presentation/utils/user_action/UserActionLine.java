package cz.cervenks.pia.application.presentation.utils.user_action;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class UserActionLine {

    private final ButtonFactory buttonFactory;

    @Getter
    private String username;

    @Getter
    private String email;

    @Getter
    private boolean hasText = false;

    @Getter
    private String text = "";

    @Getter
    private List<UserActionButton> buttons = new ArrayList<>();

    public UserActionLine(ButtonFactory buttonFactory, String username, String email) {
        this.buttonFactory = buttonFactory;
        this.username = username;
        this.email = email;
    }

    public void addButton(ButtonType type) {
        UserActionButton button = this.buttonFactory.makeButton(this, type);
        this.buttons.add(button);
    }

    public void displayResult(String text) {
        this.text = text;
        this.hasText = true;
    }

}

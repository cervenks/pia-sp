package cz.cervenks.pia.application.presentation.controllers.impl;

import cz.cervenks.pia.application.presentation.controllers.api.RegistrationController;
import cz.cervenks.pia.business.usecases.validation.api.FieldValidator;
import cz.cervenks.pia.business.usecases.validation.implementation.ValidationResult;
import cz.cervenks.pia.application.presentation.utils.PasswordPair;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/validate")
@RequiredArgsConstructor
public class RegistrationControllerImpl implements RegistrationController {

    private final FieldValidator fieldValidator;

    @Override
    @PostMapping("/email")
    public ValidationResult validateEmail(@RequestBody String value) {
        return fieldValidator.validateEmail(value);
    }

    @Override
    @PostMapping("/username")
    public ValidationResult validateUsername(@RequestBody String value) {
        return fieldValidator.validateUsername(value);
    }

    @Override
    @PostMapping("/password")
    public ValidationResult validatePassword(@RequestBody PasswordPair passwordPair) {
        String password = passwordPair.getPassword();
        String passwordCheck = passwordPair.getPasswordCheck();
        return fieldValidator.validatePassword(password, passwordCheck);
    }


}

package cz.cervenks.pia.application.presentation.utils.user_action;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.relationship.api.ActionType;
import cz.cervenks.pia.business.usecases.relationship.api.RelationshipManagement;
import cz.cervenks.pia.business.usecases.relationship.result.FriendRequestResult;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class UserActionButton {

    protected final RelationshipManagement relationshipManagement;
    protected final LoggedUserService loggedUserService;
    protected final UserActionLine parentLine;

    public abstract String getButtonText();
    public abstract String getButtonClass();
    protected abstract ActionType getActionType();
    protected abstract String getSuccessMessage();


    public void doAction() {
        String invokerEmail = loggedUserService.getUser().getUsername();
        String targetEmail = parentLine.getEmail();
        ActionType actionType = getActionType();

        FriendRequestResult result = relationshipManagement.proceedAction(invokerEmail, targetEmail, actionType);

        String message;
        if(result == FriendRequestResult.OK) {
            message = getSuccessMessage();
        } else {
            message = getErrorMessage(result);
        }
        this.parentLine.displayResult(message);
    }





    private String getErrorMessage(FriendRequestResult result) {
        switch(result) {
            case BLOCKED_BY_HIM:
                return "Chyba: Tento uživatel vás zablokoval.";
            case BLOCKED_BY_ME:
                return "Chyba: Toho uživatele blokujete.";
            case REQUESTED_BY_HIM:
                return "Chyba: Uživatel vám poslal žádost o přátelství.";
            case REQUESTED_BY_ME:
                return "Chyba: Uživatele žádáte o přátelství.";
            case FRIENDS:
                return "Chyba: S tímto uživatelem jste přátelé";
            case NO_RELATIONSHIP:
                return "Chyba: Neexistuje vztah mezi vámi a uživatelem.";
            case INTERNAL_ERROR:
                return "Chyba: Neočekávaná chyba.";
        }

        return "Chyba: Neznámá chyba.";
    }

}

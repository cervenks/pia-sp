package cz.cervenks.pia.application.presentation.auth;

import cz.cervenks.pia.application.configuration.WebsocketPaths;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.messaging.simp.SimpMessageSendingOperations;

import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
public class LoggedUserInfo {

    private String userEmail;

    private final SimpMessageSendingOperations simpMessageSendingOperations;


    public void addFriend(String friendEmail) {
        sendUpdate(AuthType.LOGIN, friendEmail);
    }

    public void removeFriend(String friendEmail) {
        sendUpdate(AuthType.LOGOUT, friendEmail);
    }


    private void sendUpdate(AuthType authType, String friendEmail) {
        FriendsUpdateMessage friendsUpdateMessage = new FriendsUpdateMessage(friendEmail, friendEmail, authType);
        simpMessageSendingOperations.convertAndSendToUser(
                userEmail, WebsocketPaths.FRIEND_UPDATE_QUEUE, friendsUpdateMessage);
    }



}

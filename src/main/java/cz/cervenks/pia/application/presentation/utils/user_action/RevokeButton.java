package cz.cervenks.pia.application.presentation.utils.user_action;

import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.business.usecases.relationship.api.ActionType;
import cz.cervenks.pia.business.usecases.relationship.api.RelationshipManagement;

public class RevokeButton extends UserActionButton {

    public RevokeButton(RelationshipManagement relationshipManagement, LoggedUserService loggedUserService, UserActionLine parentLine) {
        super(relationshipManagement, loggedUserService, parentLine);
    }

    @Override
    protected ActionType getActionType() {
        return ActionType.REVOKE;
    }

    @Override
    protected String getSuccessMessage() {
        return "Žádost o přátelství zrušena.";
    }

    @Override
    public String getButtonText() {
        return "Zrušit žádost";
    }

    @Override
    public String getButtonClass() {
        return "btn-warning";
    }
}

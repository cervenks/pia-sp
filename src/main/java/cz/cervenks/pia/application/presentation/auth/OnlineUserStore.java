package cz.cervenks.pia.application.presentation.auth;

import cz.cervenks.pia.business.domain.User;
import cz.cervenks.pia.business.usecases.relationship.api.UserSearch;
import cz.cervenks.pia.business.usecases.relationship.result.UserWithState;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
@RequiredArgsConstructor
public class OnlineUserStore {

    private Map<String, LoggedUserInfo> loggedUsers = new HashMap<>();
    private final UserSearch userSearch;
    private final SimpMessageSendingOperations simpMessageSendingOperations;

    public void addUser(String email) {
        if(!loggedUsers.containsKey(email)) {
            addNonExistingUser(email);
        }
    }

    public void removeUser(String email) {
        Set<String> onlineFriends = getOnlineFriends(email);
        removeUserFromMap(email);
        updateFriendsInfoOnRemove(email, onlineFriends); //send all his friends message that he logged out
    }

    public Collection<String> readUser(String email) {
        return  getOnlineFriends(email);
    }

    private void addNonExistingUser(String email) {
        Set<String> onlineFriends = getOnlineFriends(email);
        addUserToMap(email);
        updateFriendsInfoOnAdd(email, onlineFriends); //send all his friends message that he logged in
    }

    private void addUserToMap(String email) {
        LoggedUserInfo userInfo = new LoggedUserInfo(email, simpMessageSendingOperations);
        loggedUsers.put(email, userInfo);
    }

    private void updateFriendsInfoOnAdd(String email, Set<String> onlineFriends) {
        for(String friend : onlineFriends) {
            LoggedUserInfo userInfo = loggedUsers.get(friend);
            userInfo.addFriend(email);
        }
    }


    private void removeUserFromMap(String email) {
        loggedUsers.remove(email);
    }

    private void updateFriendsInfoOnRemove(String email, Set<String> onlineFriends) {
        for(String friend : onlineFriends) {
            LoggedUserInfo userInfo = loggedUsers.get(friend);
            userInfo.removeFriend(email);
        }
    }

    private Set<String> getOnlineFriends(String userEmail) {
        List<UserWithState> usersWithState = userSearch.listFriends(userEmail);
        Set<String> result = extractEmails(usersWithState);
        Set<String> onlineUserEmails = loggedUsers.keySet();
        result.retainAll(onlineUserEmails);
        return result;
    }

    private Set<String> extractEmails(List<UserWithState> users) {
        Set<String> result = new HashSet<>();
        for(UserWithState userWithState : users) {
            User user = userWithState.getUser();
            String email = user.getEmail();
            result.add(email);
        }
        return result;
    }



}

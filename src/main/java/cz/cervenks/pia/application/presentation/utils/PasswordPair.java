package cz.cervenks.pia.application.presentation.utils;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PasswordPair {
    private String password;
    private String passwordCheck;
}

package cz.cervenks.pia.application.presentation.utils.feed;

import cz.cervenks.pia.application.presentation.utils.feed.PostButton;
import cz.cervenks.pia.business.domain.User;
import cz.cervenks.pia.business.usecases.feed.result.LikeState;
import cz.cervenks.pia.business.usecases.feed.result.PostInfo;
import lombok.Getter;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.UUID;


public class PostView extends PostInfo {

    @Getter
    private PostButton postButton;

    private final PostButtonFactory postButtonFactory;

    public PostView(PostInfo another, PostButtonFactory postButtonFactory) {
        super(another);
        this.postButtonFactory = postButtonFactory;
        this.postButton = postButtonFactory.makeButton(this);
    }

    public String getFormattedDate() {
        String pattern = "EEEEE, dd. MMMMM yyyy, HH:mm:ss";
        Locale locale = new Locale("cs", "CZ");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, locale);
        Timestamp timestamp = getTimestamp();
        return simpleDateFormat.format(timestamp);
    }

    public String getFormattedLikes() {
        long likes = getLikes();
        if(likes == 0) {
            return "Příspěvek se nelíbí žádnému uživateli.";
        } else if(likes == 1) {
            return "1 uživateli se to líbí.";
        } else {
            return "" + likes + " uživatelům se to líbí.";
        }

    }

    public void refreshLikesFrom(PostInfo postInfo) {
        List<User> users = postInfo.getUsersLiked();
        int likes = postInfo.getLikes();
        LikeState likeState = postInfo.getLikeState();

        this.setUsersLiked(users);
        this.setLikes(likes);
        this.setLikeState(likeState);

        this.postButton = postButtonFactory.makeButton(this);
    }

}

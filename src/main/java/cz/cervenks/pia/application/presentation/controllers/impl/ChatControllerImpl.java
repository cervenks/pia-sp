package cz.cervenks.pia.application.presentation.controllers.impl;

import cz.cervenks.pia.application.configuration.WebsocketPaths;
import cz.cervenks.pia.application.presentation.controllers.api.ChatController;
import cz.cervenks.pia.application.presentation.service.api.LoggedUserService;
import cz.cervenks.pia.application.presentation.utils.ChatMessageReceive;
import cz.cervenks.pia.application.presentation.utils.ChatMessageSend;
import cz.cervenks.pia.business.usecases.chat.api.Chat;
import cz.cervenks.pia.business.usecases.chat.result.MessageInfo;
import cz.cervenks.pia.business.usecases.chat.result.SendMessageResult;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/chat")
@RequiredArgsConstructor
public class ChatControllerImpl implements ChatController {

    private final LoggedUserService loggedUserService;
    private final Chat chat; //business class
    private final SimpMessageSendingOperations simpMessageSendingOperations;

    @Override
    @PutMapping("/send")
    public void sendMessage(@RequestBody ChatMessageSend chatMessageSend) {
        String sender = loggedUserService.getUser().getUsername();
        String receiver = chatMessageSend.getWhom();
        String text = chatMessageSend.getText();
        SendMessageResult sendMessageResult = chat.sendMessage(sender, receiver, text);

        if(sendMessageResult == SendMessageResult.OK) {
            Timestamp timestamp = Timestamp.from(Instant.now());
            sendCallbackMessages(sender, receiver, text, timestamp);
        }

    }

    @Override
    @GetMapping("/history/{otherUser}")
    public List<ChatMessageReceive> getChatHistory(@PathVariable("otherUser") String otherUser) {
        String sender = loggedUserService.getUser().getUsername();
        List<MessageInfo> messageInfos = chat.getChatHistory(sender, otherUser);
        return mapReceiveMessages(otherUser, messageInfos);
    }

    private void sendCallbackMessages(String sender, String receiver, String text, Timestamp timestamp) {
        ChatMessageReceive messageToSender = new ChatMessageReceive(text, receiver, timestamp, true);
        ChatMessageReceive messageToReceiver = new ChatMessageReceive(text, sender, timestamp, false);
        simpMessageSendingOperations.convertAndSendToUser(
                sender, WebsocketPaths.MESSAGE_RECEIVE_QUEUE, messageToSender);

        simpMessageSendingOperations.convertAndSendToUser(
                receiver, WebsocketPaths.MESSAGE_RECEIVE_QUEUE, messageToReceiver);

    }

    private List<ChatMessageReceive> mapReceiveMessages(String otherUser, List<MessageInfo> infos) {
        List<ChatMessageReceive> result = new ArrayList<>();
        for(MessageInfo messageInfo : infos) {
            ChatMessageReceive chatMessageReceive = ChatMessageReceive.fromMessageInfo(messageInfo, otherUser);
            result.add(chatMessageReceive);
        }
        return result;
    }

}

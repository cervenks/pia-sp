package cz.cervenks.pia.application.presentation.utils.role_action;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RoleActionLineFactory {

    private final RoleButtonFactory roleButtonFactory;

    public RoleActionLine makeGrantAdminLine(String username, String email) {
        RoleActionLine roleActionLine = new RoleActionLine(roleButtonFactory, username, email);
        roleActionLine.addGrantAdminButton();;
        return roleActionLine;
    }

    public RoleActionLine makeRevokeAdminLine(String username, String email) {
        RoleActionLine roleActionLine = new RoleActionLine(roleButtonFactory, username, email);
        roleActionLine.addRevokeAdminButton();
        return roleActionLine;
    }

}

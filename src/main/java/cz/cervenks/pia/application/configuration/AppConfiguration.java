package cz.cervenks.pia.application.configuration;

import javax.servlet.ServletContext;

import cz.cervenks.pia.application.presentation.auth.OnlineUserStore;
import org.ocpsoft.rewrite.config.ConfigurationBuilder;
import org.ocpsoft.rewrite.servlet.config.DispatchType;
import org.ocpsoft.rewrite.servlet.config.HttpConfigurationProvider;
import org.ocpsoft.rewrite.servlet.config.Path;
import org.ocpsoft.rewrite.servlet.config.SendStatus;
import org.ocpsoft.rewrite.servlet.config.rule.Join;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class AppConfiguration extends HttpConfigurationProvider {
    @Override
    public org.ocpsoft.rewrite.config.Configuration getConfiguration(ServletContext context) {
        return ConfigurationBuilder.begin()
                .addRule().when(DispatchType.isRequest().and(Path.matches("/{path}")))
                .perform(SendStatus.code(404))
                .where("path").matches("^/jsf/.*\\.xhtml$")
                .addRule(Join.path("/").to("/jsf/dashboard.xhtml"))
                .addRule(Join.path("/login").to("/jsf/login.xhtml"))
                .addRule(Join.path("/login_error").to("/jsf/login_error.xhtml"))
                .addRule(Join.path("/register").to("/jsf/register.xhtml"))
                .addRule(Join.path("/relationship").to("/jsf/relationship.xhtml"))
                .addRule(Join.path("/dashboard").to("/jsf/dashboard.xhtml"))
                .addRule(Join.path("/friend_list").to("/jsf/friend_list.xhtml"))
                .addRule(Join.path("/admin").to("/jsf/admin_management.xhtml"))
                .addRule(Join.path("/register_success").to("/jsf/register_success.xhtml"));


    }

    @Override
    public int priority() {
        return 10;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

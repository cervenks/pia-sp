package cz.cervenks.pia.application.configuration;

import cz.cervenks.pia.application.data.repository.UserRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@EnableJpaRepositories(basePackageClasses = {
        UserRepository.class
})
@EnableTransactionManagement
public class JpaConfiguration {

}
package cz.cervenks.pia.application.configuration;

import cz.cervenks.pia.application.presentation.auth.LoginSuccessHandler;
import cz.cervenks.pia.application.presentation.auth.LogoutSuccessHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;


import lombok.RequiredArgsConstructor;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        jsr250Enabled = true,
        prePostEnabled = true,
        securedEnabled = true
)
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


    private final UserDetailsService uds;
    private final LoginSuccessHandler loginSuccessHandler;
    private final LogoutSuccessHandler logoutSuccessHandler;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests(auth -> auth
                        .antMatchers("/webjars/**").permitAll()
                        .antMatchers("/css/**").permitAll()
                        .antMatchers("/js/**").permitAll()
                        .antMatchers("/javax.faces.resource/**").permitAll()
                        .antMatchers("/dashboard").hasRole("USER")
                        .antMatchers("/relationship").hasRole("USER")
                        .antMatchers("/admin").hasRole("ADMIN")
                        .antMatchers("/login").not().authenticated()
                        .antMatchers("/login_error").not().authenticated()
                        .antMatchers("/register").not().authenticated()
                        .antMatchers("/register_success").not().authenticated()
                        .antMatchers("/api/validate/**").permitAll()
                        .anyRequest().authenticated()
                )
                .userDetailsService(this.uds)
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/dashboard", true)
                .failureUrl("/login_error")
                .successHandler(loginSuccessHandler)
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login")
                .logoutSuccessHandler(logoutSuccessHandler)
                .and()
                .headers()
                .xssProtection()

        ;
    }

}
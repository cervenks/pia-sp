package cz.cervenks.pia.application.configuration;

import cz.cervenks.pia.business.usecases.admin.api.SuperAdminCreator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class DbConfiguration implements InitializingBean {

    private final static String SUPER_ADMIN_EMAIL = "superadmin@admin.com";
    private final static String SUPER_ADMIN_USERNAME = "superadmin";
    private final static String SUPER_ADMIN_PASSWORD = "password";

    private final SuperAdminCreator superAdminCreator;

    @Override
    public void afterPropertiesSet() throws Exception {
        setupAdminUser();
    }

    private void setupAdminUser() {
        superAdminCreator.setupStartupAdmin(SUPER_ADMIN_EMAIL, SUPER_ADMIN_USERNAME, SUPER_ADMIN_PASSWORD);
        log.debug("Admin account = Email: " + SUPER_ADMIN_EMAIL + ", Password: " + SUPER_ADMIN_PASSWORD);
    }

}
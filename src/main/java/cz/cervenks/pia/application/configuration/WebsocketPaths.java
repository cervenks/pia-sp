package cz.cervenks.pia.application.configuration;

import org.springframework.stereotype.Component;

public class WebsocketPaths {

    public final static String FRIEND_UPDATE_QUEUE = "/queue/friends";
    public final static String MESSAGE_RECEIVE_QUEUE = "/queue/messages";
}

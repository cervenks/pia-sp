package cz.cervenks.pia.application.configuration;

import lombok.Getter;
import org.springframework.stereotype.Component;

@Component("applicationVariables")
public class ApplicationVariables {

    //password with entropy less than 40 will not be validated
    @Getter
    private final double minPasswordEntropy = 40.0;
}

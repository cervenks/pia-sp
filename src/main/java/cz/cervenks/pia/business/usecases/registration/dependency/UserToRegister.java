package cz.cervenks.pia.business.usecases.registration.dependency;

public interface UserToRegister {
    String getUsername();
    String getEmail();
    String getPassword();
    String getPasswordCheck();
}

package cz.cervenks.pia.business.usecases.feed.result;

public enum LikeState {
    LIKED, NOT_LIKED, CANT_LIKE
}

package cz.cervenks.pia.business.usecases.validation.implementation;

import cz.cervenks.pia.application.configuration.ApplicationVariables;
import cz.cervenks.pia.business.usecases.validation.api.FieldValidator;
import cz.cervenks.pia.business.usecases.validation.api.RequirementValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component("fieldValidator")
@RequiredArgsConstructor
public class FieldValidatorImpl implements FieldValidator {

    private final RequirementValidator requirementValidator;
    private final ApplicationVariables applicationVariables;

    @Override
    public ValidationResult validateUsername(String value) {
        if(!requirementValidator.usernameContainsCharacters(value)) {
            return new ValidationResult(false, "Uživatelské jméno je prázdné.");
        }
        if(!requirementValidator.validateUsernameContent(value)) {
            return new ValidationResult(false, "Uživatelské jméno může obsahovat pouze alfanumerické znaky a podtržítko.");
        }
        if(!requirementValidator.validateMaxLength(value, 20)) {
            return new ValidationResult(false, "Uživatelské jméno může mít maximálně 20 znaků.");
        }
        return new ValidationResult(true, "Uživatelské jméno je v pořádku.");
    }

    @Override
    public ValidationResult validateEmail(String value) {
        if(!requirementValidator.validateEmailRegex(value)) {
            return new ValidationResult(false, "Zadejte platnou emailovou adresu.");
        }
        if(!requirementValidator.emailAvailable(value)) {
            return new ValidationResult(false, "Emailová adresa již zaregistrována.");
        }
        if(!requirementValidator.validateMaxLength(value, 150)) {
            return new ValidationResult(false, "Emailová adresa může mít maximálně 150 znaků.");
        }
        return new ValidationResult(true, "Email je v pořádku.");
    }

    @Override
    public ValidationResult validatePassword(String password, String passwordCheck) {
        ValidationResult entropyResult = checkPasswordEntropy(password);

        if(!entropyResult.isValid()) {
            return new ValidationResult(false, entropyResult.getValidationMessage());
        }

        if(!requirementValidator.checkPasswordEquality(password, passwordCheck)) {
            return new ValidationResult(false, "Hesla se neshodují.");
        }

        if(!requirementValidator.validateMaxLength(password, 30)) {
            return new ValidationResult(false, "Heslo může mít maximálně 30 znaků.");
        }

        return new ValidationResult(true, entropyResult.getValidationMessage());
    }

    private ValidationResult checkPasswordEntropy(String password) {
        double entropy = requirementValidator.calculatePasswordEntropy(password);
        boolean valid = entropy >= applicationVariables.getMinPasswordEntropy();
        String message;
        if(entropy < 20) {
            message = "velmi slabé";
        } else if(entropy < 40) {
            message = "slabé";
        } else if(entropy < 60) {
            message = "dostačující";
        } else if (entropy < 80) {
            message = "silné";
        } else {
            message = "velmi silné";
        }

        return new ValidationResult(valid, String.format("Heslo je %s (%.2f bitů)", message, entropy));
    }
}

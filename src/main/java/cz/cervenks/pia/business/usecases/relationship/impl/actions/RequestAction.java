package cz.cervenks.pia.business.usecases.relationship.impl.actions;

import cz.cervenks.pia.business.domain.RelationshipState;
import cz.cervenks.pia.business.model.RelationshipModel;

import java.util.Collection;

public class RequestAction extends RelationshipAction {

    public RequestAction(RelationshipModel relationshipModel, String userEmail1, String userEmail2) {
        super(relationshipModel, userEmail1, userEmail2);
    }

    @Override
    protected Collection<RelationshipState> getValidStates() {
        return getStates(RelationshipState.NO_RELATIONSHIP, RelationshipState.BLOCKED_BY_ME);
    }

    @Override
    protected RelationshipState getTargetState() {
        return RelationshipState.REQUESTED_BY_ME;
    }
}

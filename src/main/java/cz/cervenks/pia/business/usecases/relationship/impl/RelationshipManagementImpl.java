package cz.cervenks.pia.business.usecases.relationship.impl;

import cz.cervenks.pia.business.model.RelationshipModel;
import cz.cervenks.pia.business.usecases.relationship.api.ActionType;
import cz.cervenks.pia.business.usecases.relationship.api.RelationshipManagement;
import cz.cervenks.pia.business.usecases.relationship.impl.actions.*;
import cz.cervenks.pia.business.usecases.relationship.result.FriendRequestResult;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component("relationshipManagement")
@RequiredArgsConstructor
public class RelationshipManagementImpl implements RelationshipManagement {

    private final RelationshipModel relationshipModel;

    @Override
    public FriendRequestResult proceedAction(String userEmail1, String userEmail2, ActionType type) {
        RelationshipAction action = makeAction(userEmail1, userEmail2, type);
        if(action == null) {
            return FriendRequestResult.INTERNAL_ERROR;
        }
        return action.proceedAction();
    }

    private RelationshipAction makeAction(String userEmail1, String userEmail2, ActionType type) {
        switch(type) {
            case ACCEPT:
                return new AcceptAction(relationshipModel, userEmail1, userEmail2);
            case BLOCK:
                return new BlockAction(relationshipModel, userEmail1, userEmail2);
            case REJECT:
                return new RejectAction(relationshipModel, userEmail1, userEmail2);
            case REQUEST:
                return new RequestAction(relationshipModel, userEmail1, userEmail2);
            case REVOKE:
                return new RevokeAction(relationshipModel, userEmail1, userEmail2);
            case UNBLOCK:
                return new UnblockAction(relationshipModel, userEmail1, userEmail2);
            case UNFRIEND:
                return new UnfriendAction(relationshipModel, userEmail1, userEmail2);
            default:
                return null;
        }
    }
}

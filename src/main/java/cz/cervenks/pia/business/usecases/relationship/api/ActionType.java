package cz.cervenks.pia.business.usecases.relationship.api;

public enum ActionType {
    ACCEPT, BLOCK, REJECT, REQUEST, REVOKE, UNBLOCK, UNFRIEND
}

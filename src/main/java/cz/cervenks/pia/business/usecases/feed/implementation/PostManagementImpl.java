package cz.cervenks.pia.business.usecases.feed.implementation;

import cz.cervenks.pia.business.domain.Post;
import cz.cervenks.pia.business.domain.factory.PostFactory;
import cz.cervenks.pia.business.model.PostModel;
import cz.cervenks.pia.business.usecases.feed.api.PostManagement;
import cz.cervenks.pia.business.usecases.feed.result.PostInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Component("postManagement")
@RequiredArgsConstructor
public class PostManagementImpl implements PostManagement {

    private final PostModel postModel;
    private final PostFactory postFactory;

    @Override
    public List<PostInfo> showPosts(int limit, String userEmail, Timestamp timestamp) {
        return postModel.getPostsFor(userEmail, timestamp, limit);
    }

    @Override
    public void newPost(String authorEmail, String text) {
        Timestamp timestamp = Timestamp.from(Instant.now());
        Post post = postFactory.makePost(authorEmail, timestamp, text, false);
        postModel.addPost(post);
    }

    @Override
    public PostInfo toggleLike(UUID postId, String userEmail) {
        postModel.toggleLike(postId, userEmail);
        return postModel.getPost(userEmail, postId);
    }
}

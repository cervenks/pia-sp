package cz.cervenks.pia.business.usecases.relationship.result;

import cz.cervenks.pia.business.domain.RelationshipState;
import cz.cervenks.pia.business.domain.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;


@AllArgsConstructor
public class UserWithState {

    @Getter
    private User user;

    @Getter
    @Setter
    private RelationshipState state;


}

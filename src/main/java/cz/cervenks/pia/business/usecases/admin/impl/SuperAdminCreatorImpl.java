package cz.cervenks.pia.business.usecases.admin.impl;

import cz.cervenks.pia.business.domain.Role;
import cz.cervenks.pia.business.domain.User;
import cz.cervenks.pia.business.model.RoleModel;
import cz.cervenks.pia.business.model.UserModel;
import cz.cervenks.pia.business.usecases.admin.api.SuperAdminCreator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
@AllArgsConstructor
public class SuperAdminCreatorImpl implements SuperAdminCreator {

    private final UserModel userModel;
    private final RoleModel roleModel;


    @Override
    public void setupStartupAdmin(String email, String username, String password) {
        User user = userModel.getIfExists(email);
        if(user == null) {
            createNewAdmin(email, username, password);
            return;
        }

        boolean isAdmin = user.hasRole(Role.ADMIN);
        if(!isAdmin) {
            roleModel.addRole(email, Role.ADMIN);
        }
    }

    private void createNewAdmin(String email, String username, String password) {
        Set<Role> roles = getAdminRoles();
        User user = new User(username, email, password, roles);
        userModel.addUser(user);
    }

    private Set<Role> getAdminRoles() {
        Set<Role> result = new HashSet<>();
        result.add(Role.USER);
        result.add(Role.ADMIN);
        return result;
    }
}

package cz.cervenks.pia.business.usecases.validation.implementation;

import cz.cervenks.pia.application.data.entity.UserEntity;
import cz.cervenks.pia.application.data.repository.UserRepository;
import cz.cervenks.pia.business.usecases.validation.api.RequirementValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component("validator")
@RequiredArgsConstructor
public class RequirementValidatorImpl implements RequirementValidator {

    private final UserRepository userRepository;
    private final RegexHolder regexHolder;


    private CharacterPool[] characterPools;

    @PostConstruct
    private void setupPools() {
        this.characterPools = new CharacterPool[] {
                new CharacterPool(26, regexHolder.getLowerCaseCharacterRegex()),
                new CharacterPool(26, regexHolder.getUpperCaseCharacterRegex()),
                new CharacterPool(10, regexHolder.getNumberCharacterRegex()),
                new CharacterPool(33, regexHolder.getSymbolRegex())
        };
    }

    @Override
    public boolean validateEmailRegex(String email) {
        String emailRegex = regexHolder.getEmailRegex();
        Pattern pattern = Pattern.compile(emailRegex);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public boolean emailAvailable(String email) {
        Optional<UserEntity> queryResult = userRepository.findByEmail(email);
        return queryResult.isEmpty();
    }

    @Override
    public double calculatePasswordEntropy(String password) {
        int poolSize = getPoolSize(password);
        int length = password.length();

        if(length == 0 || poolSize == 0) {
            return 0;
        }

        return (Math.log(poolSize) / Math.log(2)) * length;
    }

    @Override
    public boolean checkPasswordEquality(String password, String passwordCheck) {
        return password.equals(passwordCheck);
    }

    @Override
    public boolean usernameContainsCharacters(String username) {
        if(username == null) {
            return false;
        }
        return !username.isBlank();
    }

    @Override
    public boolean validateUsernameContent(String username) {
        String usernameRegex = regexHolder.getUsernameRegex();
        Pattern pattern = Pattern.compile(usernameRegex);
        Matcher matcher = pattern.matcher(username);
        return matcher.matches();
    }

    @Override
    public boolean validateMaxLength(String input, int maxLength) {
        int inputLength = input.length();
        return inputLength <= maxLength;
    }


    private int getPoolSize(String password) {
        int result = 0;
        for(CharacterPool pool : characterPools) {
            if(pool.found(password)) {
                result += pool.getSize();
            }
        }
        return result;
    }


}

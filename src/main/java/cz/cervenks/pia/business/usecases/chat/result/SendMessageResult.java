package cz.cervenks.pia.business.usecases.chat.result;

public enum SendMessageResult {
    OK, NOT_FRIENDS
}

package cz.cervenks.pia.business.usecases.validation.api;

public interface RequirementValidator {
    boolean validateEmailRegex(String email);
    boolean emailAvailable(String email);
    double calculatePasswordEntropy(String password);
    boolean checkPasswordEquality(String password, String passwordCheck);
    boolean usernameContainsCharacters(String username);
    boolean validateUsernameContent(String username);
    boolean validateMaxLength(String input, int maxLength);
}

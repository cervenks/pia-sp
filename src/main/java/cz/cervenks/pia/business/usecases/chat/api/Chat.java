package cz.cervenks.pia.business.usecases.chat.api;

import cz.cervenks.pia.business.usecases.chat.result.MessageInfo;
import cz.cervenks.pia.business.usecases.chat.result.SendMessageResult;

import java.util.List;

public interface Chat {

    SendMessageResult sendMessage(String by, String whom, String text);
    List<MessageInfo> getChatHistory(String invoker, String target);
}

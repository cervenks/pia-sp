package cz.cervenks.pia.business.usecases.validation.implementation;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@AllArgsConstructor
public class CharacterPool {

    @Getter
    private int size;
    private String regex;

    public boolean found(String input) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        return matcher.find();
    }
}

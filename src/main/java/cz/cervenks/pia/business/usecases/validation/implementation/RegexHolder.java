package cz.cervenks.pia.business.usecases.validation.implementation;

import lombok.Getter;
import org.springframework.stereotype.Component;

@Component("regexHolder")
public class RegexHolder {

    @Getter
    private final String emailRegex =
            "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)" +
                    "*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\" +
                    "x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:" +
                    "(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-" +
                    "]*[a-z0-9])?|\\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1" +
                    "-9]?[0-9]))\\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-" +
                    "9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0" +
                    "e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\" +
                    "x0e-\\x7f])+)\\])";

    @Getter
    private final String lowerCaseCharacterRegex = "[a-z]";
    @Getter
    private final String upperCaseCharacterRegex = "[A-Z]";
    @Getter
    private final String numberCharacterRegex = "[0-9]";
    @Getter
    private final String symbolRegex = "[^a-zA-Z0-9]";
    @Getter
    private final String usernameRegex = "[0-9A-Za-z_]*";
}

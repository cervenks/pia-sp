package cz.cervenks.pia.business.usecases.relationship.impl.actions;

import cz.cervenks.pia.business.domain.RelationshipState;
import cz.cervenks.pia.business.model.RelationshipModel;
import cz.cervenks.pia.business.usecases.relationship.result.FriendRequestResult;
import lombok.AllArgsConstructor;

import java.util.Arrays;
import java.util.Collection;

@AllArgsConstructor
public abstract class RelationshipAction {

    private final RelationshipModel relationshipModel;

    protected String userEmail1;
    protected String userEmail2;

    protected abstract Collection<RelationshipState> getValidStates();
    protected abstract RelationshipState getTargetState();


    public FriendRequestResult proceedAction() {
        FriendRequestResult stateCheck = checkCorrectState();
        if(stateCheck != FriendRequestResult.OK) {
            return stateCheck;
        }

        setCorrectState();
        return FriendRequestResult.OK;
    }

    protected Collection<RelationshipState> getStates(RelationshipState... states) {
        return Arrays.asList(states);
    }

    private FriendRequestResult checkCorrectState() {
        Collection<RelationshipState> validStates = getValidStates();
        RelationshipState currentState = relationshipModel.getState(userEmail1, userEmail2);

        if(validStates.contains(currentState)) {
            return FriendRequestResult.OK;
        } else {
            return convertToErrorState(currentState);
        }
    }

    private void setCorrectState() {
        RelationshipState targetState = getTargetState();
        relationshipModel.setBidirectionalState(userEmail1, userEmail2, targetState);
    }

    private FriendRequestResult convertToErrorState(RelationshipState status) {
        switch(status) {
            case NO_RELATIONSHIP:
                return FriendRequestResult.NO_RELATIONSHIP;
            case REQUESTED_BY_ME:
                return FriendRequestResult.REQUESTED_BY_ME;
            case REQUESTED_BY_HIM:
                return FriendRequestResult.REQUESTED_BY_HIM;
            case FRIENDS:
                return FriendRequestResult.FRIENDS;
            case BLOCKED_BY_ME:
                return FriendRequestResult.BLOCKED_BY_ME;
            case BLOCKED_BY_HIM:
                return FriendRequestResult.BLOCKED_BY_HIM;
            default:
                return FriendRequestResult.INTERNAL_ERROR;
        }
    }


}

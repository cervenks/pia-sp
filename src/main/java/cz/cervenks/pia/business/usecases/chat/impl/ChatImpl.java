package cz.cervenks.pia.business.usecases.chat.impl;

import cz.cervenks.pia.business.configuration.BusinessVariables;
import cz.cervenks.pia.business.domain.Message;
import cz.cervenks.pia.business.domain.RelationshipState;
import cz.cervenks.pia.business.model.MessageModel;
import cz.cervenks.pia.business.model.RelationshipModel;
import cz.cervenks.pia.business.usecases.chat.api.Chat;
import cz.cervenks.pia.business.usecases.chat.result.MessageInfo;
import cz.cervenks.pia.business.usecases.chat.result.SendMessageResult;
import cz.cervenks.pia.business.usecases.relationship.api.RelationshipManagement;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;

@Component("chat")
@RequiredArgsConstructor
public class ChatImpl implements Chat {

    private final MessageModel messageModel;
    private final BusinessVariables businessVariables;
    private final RelationshipModel relationshipModel;

    @Override
    public SendMessageResult sendMessage(String senderEmail, String receiverEmail, String text) {

        RelationshipState state = relationshipModel.getState(senderEmail, receiverEmail);
        if(state != RelationshipState.FRIENDS) {
            return SendMessageResult.NOT_FRIENDS;
        }

        Timestamp timestamp = Timestamp.from(Instant.now());
        Message message = new Message(senderEmail, receiverEmail, timestamp, text);
        messageModel.createMessage(message);
        return SendMessageResult.OK;
    }

    @Override
    public List<MessageInfo> getChatHistory(String invoker, String target) {
        int limit = businessVariables.getChatHistoryLimit();
        return messageModel.getChatHistory(invoker, target, limit);
    }
}

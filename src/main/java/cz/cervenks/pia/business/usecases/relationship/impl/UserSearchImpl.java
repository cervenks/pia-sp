package cz.cervenks.pia.business.usecases.relationship.impl;

import cz.cervenks.pia.business.domain.RelationshipState;
import cz.cervenks.pia.business.domain.User;
import cz.cervenks.pia.business.model.RelationshipModel;
import cz.cervenks.pia.business.model.UserModel;
import cz.cervenks.pia.business.usecases.relationship.api.UserSearch;
import cz.cervenks.pia.business.usecases.relationship.result.UserWithState;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Component("userSearch")
public class UserSearchImpl implements UserSearch {
    
    private final RelationshipModel relationshipModel;
    private final UserModel userModel;


    @Override
    public List<UserWithState> search(String userEmail, String searchedText) {
        List<User> allUsers = userModel.getAllUsers(userEmail, searchedText);
        List<UserWithState> userWithRelations = relationshipModel.getAllUsersWithRelation(userEmail);

        List<RelationshipState> permittedStates = getPermittedStates();
        FilteredUserList filteredUserList = new FilteredUserList(allUsers, permittedStates);
        filteredUserList.remove(userEmail); //we will not display self
        filteredUserList.filter(userWithRelations); //filter all relations

        return filteredUserList.getUsers();
    }

    @Override
    public List<UserWithState> listFriends(String userEmail) {
        return relationshipModel.getUsersByState(userEmail, RelationshipState.FRIENDS);
    }

    @Override
    public List<UserWithState> listPending(String userEmail) {
        return relationshipModel.getUsersByState(userEmail, RelationshipState.REQUESTED_BY_HIM);
    }

    @Override
    public List<UserWithState> listBlocked(String userEmail) {
        return relationshipModel.getUsersByState(userEmail, RelationshipState.BLOCKED_BY_ME);
    }

    private List<RelationshipState> getPermittedStates() {
        List<RelationshipState> result = new ArrayList<>();
        result.add(RelationshipState.NO_RELATIONSHIP);
        result.add(RelationshipState.BLOCKED_BY_ME);
        result.add(RelationshipState.REQUESTED_BY_HIM);
        result.add(RelationshipState.REQUESTED_BY_ME);
        return result;
    }
}

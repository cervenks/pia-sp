package cz.cervenks.pia.business.usecases.relationship.impl;

import cz.cervenks.pia.business.domain.RelationshipState;
import cz.cervenks.pia.business.domain.User;
import cz.cervenks.pia.business.usecases.relationship.result.UserWithState;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

public class FilteredUserList {

    private LinkedHashMap<String, UserWithState> data;
    private Collection<RelationshipState> permittedStates;

    FilteredUserList(Iterable<User> input, Collection<RelationshipState> permittedStates) {
        this.permittedStates = permittedStates;
        this.data = new LinkedHashMap<>();
        for(User user : input) {
            String email = user.getEmail();
            UserWithState userWithState = new UserWithState(user, RelationshipState.NO_RELATIONSHIP);
            data.put(email, userWithState);
        }
    }

    public List<UserWithState> getUsers() {
        return new ArrayList<>(this.data.values());
    }

    void filter(Iterable<UserWithState> users) {
        for(UserWithState user : users) {
            filterUser(user);
        }
    }

    void remove(String email) {
        data.remove(email);
    }

    private void filterUser(UserWithState user) {
        RelationshipState state = user.getState();
        if(permittedStates.contains(state)) {
            changeState(user);
        } else {
            remove(user);
        }
    }

    private void remove(UserWithState userWithState) {
        User user = userWithState.getUser();
        String email = user.getEmail();
        data.remove(email);
    }

    private void changeState(UserWithState userWithState) {
        RelationshipState relationshipState = userWithState.getState();
        User user = userWithState.getUser();
        String email = user.getEmail();

        UserWithState userFromData = this.data.get(email);
        if(userFromData != null) {
            userFromData.setState(relationshipState);
        }
    }

}



package cz.cervenks.pia.business.usecases.registration.impl;

import cz.cervenks.pia.business.domain.User;
import cz.cervenks.pia.business.domain.factory.UserFactory;
import cz.cervenks.pia.business.model.UserModel;
import cz.cervenks.pia.business.usecases.registration.api.Registration;
import cz.cervenks.pia.business.usecases.registration.dependency.UserToRegister;
import cz.cervenks.pia.business.usecases.validation.api.FieldValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("registration")
@RequiredArgsConstructor
public class RegistrationImpl implements Registration {

    private final FieldValidator fieldValidator;
    private final UserFactory userFactory;
    private final UserModel userModel;

    public RegistrationResult register(UserToRegister userToRegister) {
        RegistrationValidation validation = new RegistrationValidation(userToRegister, fieldValidator);
        validation.validate();

        List<String> errors = validation.getErrors();
        if(!validation.isWithoutErrors()) {
            return new RegistrationResult(RegistrationResultStatus.VALIDATION_FAILED, errors);
        }

        User newUser = userFactory.makeUser(userToRegister);
        userModel.addUser(newUser);

        return new RegistrationResult(RegistrationResultStatus.REGISTERED, errors);
    }


}

package cz.cervenks.pia.business.usecases.admin.result;

public enum AdminAssignResult {
    OK, INVOKER_NOT_ADMIN, INVOKER_SAME_AS_TARGET, ALREADY_IS
}

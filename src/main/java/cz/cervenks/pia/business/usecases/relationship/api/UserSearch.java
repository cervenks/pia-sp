package cz.cervenks.pia.business.usecases.relationship.api;


import cz.cervenks.pia.business.usecases.relationship.result.UserWithState;

import java.util.List;

public interface UserSearch {

    List<UserWithState> search(String userEmail, String searchedText);
    List<UserWithState> listFriends(String userEmail);
    List<UserWithState> listPending(String userEmail);
    List<UserWithState> listBlocked(String userEmail);
}

package cz.cervenks.pia.business.usecases.validation.implementation;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ValidationResult {

    private boolean isValid;
    private String validationMessage;
}

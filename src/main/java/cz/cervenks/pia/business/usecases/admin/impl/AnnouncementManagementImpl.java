package cz.cervenks.pia.business.usecases.admin.impl;

import cz.cervenks.pia.business.domain.Post;
import cz.cervenks.pia.business.domain.Role;
import cz.cervenks.pia.business.domain.factory.PostFactory;
import cz.cervenks.pia.business.model.PostModel;
import cz.cervenks.pia.business.model.RoleModel;
import cz.cervenks.pia.business.usecases.admin.api.AnnouncementManagement;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.time.Instant;

@Component
@RequiredArgsConstructor
public class AnnouncementManagementImpl implements AnnouncementManagement {

    private final RoleModel roleModel;
    private final PostModel postModel;
    private final PostFactory postFactory;

    @Override
    public boolean newAnnouncement(String authorEmail, String text) {
        if(!roleModel.hasRole(authorEmail, Role.ADMIN)) {
            return false;
        }

        Timestamp timestamp = Timestamp.from(Instant.now());
        Post post = postFactory.makePost(authorEmail, timestamp, text, true);
        postModel.addPost(post);
        return true;
    }
}

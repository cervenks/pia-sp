package cz.cervenks.pia.business.usecases.registration.impl;

public enum RegistrationResultStatus {
    REGISTERED, VALIDATION_FAILED
}

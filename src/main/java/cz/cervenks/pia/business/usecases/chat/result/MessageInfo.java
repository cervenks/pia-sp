package cz.cervenks.pia.business.usecases.chat.result;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
public class MessageInfo {

    private boolean sentByInvoker;
    private Timestamp timestamp;
    private String text;
}

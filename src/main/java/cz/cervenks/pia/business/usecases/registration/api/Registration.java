package cz.cervenks.pia.business.usecases.registration.api;

import cz.cervenks.pia.business.usecases.registration.dependency.UserToRegister;
import cz.cervenks.pia.business.usecases.registration.impl.RegistrationResult;

public interface Registration {

    RegistrationResult register(UserToRegister userToRegister);
}

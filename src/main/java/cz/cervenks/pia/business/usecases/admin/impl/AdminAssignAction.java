package cz.cervenks.pia.business.usecases.admin.impl;


import cz.cervenks.pia.business.domain.Role;
import cz.cervenks.pia.business.model.RoleModel;

public class AdminAssignAction extends RoleManagementAction {
    public AdminAssignAction(RoleModel roleModel, String invoker, String target) {
        super(roleModel, invoker, target);
    }

    @Override
    protected boolean doAction() {
        return roleModel.addRole(target, Role.ADMIN);
    }
}

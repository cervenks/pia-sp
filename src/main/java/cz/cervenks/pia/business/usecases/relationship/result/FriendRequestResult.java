package cz.cervenks.pia.business.usecases.relationship.result;

public enum FriendRequestResult {
    OK, NO_RELATIONSHIP, REQUESTED_BY_ME, REQUESTED_BY_HIM, FRIENDS, BLOCKED_BY_ME, BLOCKED_BY_HIM, NO_BLOCK, INTERNAL_ERROR
}

package cz.cervenks.pia.business.usecases.feed.api;

import cz.cervenks.pia.business.usecases.feed.result.PostInfo;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

public interface PostManagement {

    List<PostInfo> showPosts(int limit, String userEmail, Timestamp timestamp);
    void newPost(String authorEmail, String text);
    PostInfo toggleLike(UUID postId, String userEmail);
}

package cz.cervenks.pia.business.usecases.relationship.impl.actions;

import cz.cervenks.pia.business.domain.RelationshipState;
import cz.cervenks.pia.business.model.RelationshipModel;

import java.util.Collection;

public class AcceptAction extends RelationshipAction {
    public AcceptAction(RelationshipModel relationshipModel, String userEmail1, String userEmail2) {
        super(relationshipModel, userEmail1, userEmail2);
    }

    @Override
    protected Collection<RelationshipState> getValidStates() {
        return getStates(RelationshipState.REQUESTED_BY_HIM);
    }

    @Override
    protected RelationshipState getTargetState() {
        return RelationshipState.FRIENDS;
    }
}

package cz.cervenks.pia.business.usecases.registration.impl;

import cz.cervenks.pia.business.usecases.registration.dependency.UserToRegister;
import cz.cervenks.pia.business.usecases.validation.api.FieldValidator;
import cz.cervenks.pia.business.usecases.validation.implementation.ValidationResult;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
class RegistrationValidation {

    private final UserToRegister userToRegister;
    private final FieldValidator fieldValidator;

    @Getter
    private boolean withoutErrors = true;

    @Getter
    private List<String> errors = new ArrayList<>();

    void validate() {
        errors.add("Registrace se nezdařila.");
        validateEmail();
        validateUsername();
        validatePassword();
    }

    private void validateEmail() {
        String email = userToRegister.getEmail();
        ValidationResult validationResult = fieldValidator.validateEmail(email);
        if(!validationResult.isValid()) {
            withoutErrors = false;
            errors.add(validationResult.getValidationMessage());
        }
    }

    private void validateUsername() {
        String username = userToRegister.getUsername();
        ValidationResult validationResult = fieldValidator.validateUsername(username);
        if(!validationResult.isValid()) {
            withoutErrors = false;
            errors.add(validationResult.getValidationMessage());
        }
    }

    private void validatePassword() {
        String password = userToRegister.getPassword();
        String passwordCheck = userToRegister.getPasswordCheck();
        ValidationResult validationResult = fieldValidator.validatePassword(password, passwordCheck);
        if(!validationResult.isValid()) {
            withoutErrors = false;
            errors.add(validationResult.getValidationMessage());
        }
    }
}

package cz.cervenks.pia.business.usecases.admin.impl;

import cz.cervenks.pia.business.domain.Role;
import cz.cervenks.pia.business.model.RoleModel;
import cz.cervenks.pia.business.usecases.admin.api.RoleManagement;
import cz.cervenks.pia.business.usecases.admin.result.AdminAssignResult;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component("roleManagement")
@RequiredArgsConstructor
public class RoleManagementImpl implements RoleManagement {

    private final RoleModel roleModel;

    @Override
    public AdminAssignResult assignAdmin(String invoker, String target) {
        RoleManagementAction action = new AdminAssignAction(roleModel, invoker, target);
        return action.proceed();
    }

    @Override
    public AdminAssignResult removeAdmin(String invoker, String target) {
        RoleManagementAction action = new AdminRemoveAction(roleModel, invoker, target);
        return action.proceed();
    }


}

package cz.cervenks.pia.business.usecases.admin.impl;

import cz.cervenks.pia.business.domain.Role;
import cz.cervenks.pia.business.model.RoleModel;
import cz.cervenks.pia.business.usecases.admin.result.AdminAssignResult;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class RoleManagementAction {
    protected final RoleModel roleModel;
    protected String invoker;
    protected String target;

    public AdminAssignResult proceed() {
        if(invokerEqualsTarget()) {
            return AdminAssignResult.INVOKER_SAME_AS_TARGET;
        }

        if(!roleModel.hasRole(invoker, Role.ADMIN)) {
            return AdminAssignResult.INVOKER_NOT_ADMIN;
        }

        if(doAction()) {
            return AdminAssignResult.OK;
        } else {
            return AdminAssignResult.ALREADY_IS;
        }
    }

    protected abstract boolean doAction();

    private boolean invokerEqualsTarget() {
        return invoker.equals(target);
    }
}

package cz.cervenks.pia.business.usecases.registration.impl;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class RegistrationResult {
    private RegistrationResultStatus registrationResultStatus;
    private List<String> errors;
}

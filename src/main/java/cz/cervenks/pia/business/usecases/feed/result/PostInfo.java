package cz.cervenks.pia.business.usecases.feed.result;

import cz.cervenks.pia.business.domain.User;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

@Data
@AllArgsConstructor
public class PostInfo {

    private UUID identification;
    private User author;
    private String text;
    private Timestamp timestamp;
    private int likes;

    private List<User> usersLiked;
    private LikeState likeState;


    public PostInfo(PostInfo another) {
        this.author = another.getAuthor();
        this.identification = another.getIdentification();
        this.likes = another.getLikes();
        this.text = another.getText();
        this.timestamp = another.getTimestamp();
        this.usersLiked = another.getUsersLiked();
        this.likeState = another.getLikeState();
    }


}

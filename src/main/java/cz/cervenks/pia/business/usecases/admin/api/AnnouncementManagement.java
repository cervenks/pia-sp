package cz.cervenks.pia.business.usecases.admin.api;

public interface AnnouncementManagement {

    boolean newAnnouncement(String authorEmail, String text);
}

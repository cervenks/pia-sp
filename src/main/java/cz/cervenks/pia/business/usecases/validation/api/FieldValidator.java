package cz.cervenks.pia.business.usecases.validation.api;

import cz.cervenks.pia.business.usecases.validation.implementation.ValidationResult;

public interface FieldValidator {
    ValidationResult validateUsername(String username);
    ValidationResult validateEmail(String email);
    ValidationResult validatePassword(String password, String passwordCheck);
}

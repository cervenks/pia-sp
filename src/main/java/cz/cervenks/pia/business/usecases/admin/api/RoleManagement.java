package cz.cervenks.pia.business.usecases.admin.api;

import cz.cervenks.pia.business.usecases.admin.result.AdminAssignResult;

public interface RoleManagement {

    AdminAssignResult assignAdmin(String invoker, String target);
    AdminAssignResult removeAdmin(String invoker, String target);

}

package cz.cervenks.pia.business.usecases.admin.api;

public interface SuperAdminCreator {

    void setupStartupAdmin(String email, String username, String password);
}

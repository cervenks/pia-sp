package cz.cervenks.pia.business.usecases.relationship.api;

import cz.cervenks.pia.business.usecases.relationship.result.FriendRequestResult;

public interface RelationshipManagement {

   FriendRequestResult proceedAction(String userEmail1, String userEmail2, ActionType type);


}

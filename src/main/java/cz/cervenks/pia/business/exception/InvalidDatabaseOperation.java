package cz.cervenks.pia.business.exception;

public class InvalidDatabaseOperation extends Exception {
    public InvalidDatabaseOperation(String errorMessage) {
        super(errorMessage);
    }
}

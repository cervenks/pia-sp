package cz.cervenks.pia.business.configuration;

import lombok.Getter;
import org.springframework.stereotype.Component;

@Component("businessVariables")
public class BusinessVariables {

    @Getter
    private final int chatHistoryLimit = 15;


}

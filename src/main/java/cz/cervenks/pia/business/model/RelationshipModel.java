package cz.cervenks.pia.business.model;

import cz.cervenks.pia.business.domain.RelationshipState;
import cz.cervenks.pia.business.usecases.relationship.result.UserWithState;

import java.util.Collection;
import java.util.List;

public interface RelationshipModel {

    List<UserWithState> getUsersByState(String usersEmail, Collection<RelationshipState> states);
    List<UserWithState> getUsersByState(String userEmail, RelationshipState state);
    List<UserWithState> getAllUsersWithRelation(String userEmail);

    RelationshipState getState(String userEmail1, String userEmail2);
    void setBidirectionalState(String userEmail1, String userEmail2, RelationshipState state);
}

package cz.cervenks.pia.business.model;

import cz.cervenks.pia.business.domain.Message;
import cz.cervenks.pia.business.usecases.chat.result.MessageInfo;

import java.util.List;

public interface MessageModel {

    List<MessageInfo> getChatHistory(String invoker, String target, int limit);
    void createMessage(Message message);

}

package cz.cervenks.pia.business.model;

import cz.cervenks.pia.business.domain.Post;
import cz.cervenks.pia.business.usecases.feed.result.PostInfo;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

public interface PostModel {

    PostInfo getPost(String userEmail, UUID uuid);
    List<PostInfo> getPostsFor(String userEmail, Timestamp timestamp, int limit);
    void addPost(Post post);
    void toggleLike(UUID postId, String userEmail);

}

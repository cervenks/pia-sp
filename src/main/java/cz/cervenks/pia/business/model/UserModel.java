package cz.cervenks.pia.business.model;

import cz.cervenks.pia.business.domain.User;

import java.util.List;

public interface UserModel {
    void addUser(User user);

    User getIfExists(String email);

    List<User> getAllUsers(String userEmail, String filter);


}

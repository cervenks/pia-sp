package cz.cervenks.pia.business.model;

import cz.cervenks.pia.business.domain.Role;

public interface RoleModel {
    boolean hasRole(String user, Role role);
    boolean addRole(String user, Role role);
    boolean removeRole(String user, Role role);
}

package cz.cervenks.pia.business.domain;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Friendship {

    private User user1;
    private User user2;
    private RelationshipState status;

}

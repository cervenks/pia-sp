package cz.cervenks.pia.business.domain;

public enum RelationshipState {
    NO_RELATIONSHIP, REQUESTED_BY_ME, REQUESTED_BY_HIM, FRIENDS, BLOCKED_BY_ME, BLOCKED_BY_HIM;

    //if relationship from perspective of first user is for example in blocked_by_me state
    //relationship from perspective of other user must be in blocked_by_him state
    public RelationshipState getMirrorState() {
        switch(this) {
            case REQUESTED_BY_ME:
                return REQUESTED_BY_HIM;
            case REQUESTED_BY_HIM:
                return REQUESTED_BY_ME;
            case FRIENDS:
                return FRIENDS;
            case BLOCKED_BY_ME:
                return BLOCKED_BY_HIM;
            case BLOCKED_BY_HIM:
                return BLOCKED_BY_ME;
            case NO_RELATIONSHIP:
            default:
                return NO_RELATIONSHIP;
        }
    }
}

package cz.cervenks.pia.business.domain;


public enum Role {
    ADMIN, USER
}

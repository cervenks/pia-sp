package cz.cervenks.pia.business.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
public class Post {
    private String userEmail;
    private String text;
    private Timestamp timestamp;
    private boolean isAnnouncement;
}

package cz.cervenks.pia.business.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@Data
@AllArgsConstructor
public class User {
    private String username;
    private String email;
    private String password;
    private Set<Role> roles;

    public boolean hasRole(Role role) {
        for(Role myRole : roles) {
            if(role == myRole) {
                return true;
            }
        }
        return false;
    }
}

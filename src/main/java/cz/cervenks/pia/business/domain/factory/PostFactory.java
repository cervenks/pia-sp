package cz.cervenks.pia.business.domain.factory;

import cz.cervenks.pia.business.domain.Post;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

@Component
public class PostFactory {

    public Post makePost(String userEmail, Timestamp timestamp, String text, boolean isAnnouncement) {
        return new Post(userEmail, text, timestamp, isAnnouncement);
    }
}

package cz.cervenks.pia.business.domain.factory;

import cz.cervenks.pia.business.domain.Role;
import cz.cervenks.pia.business.domain.User;
import cz.cervenks.pia.business.usecases.registration.dependency.UserToRegister;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component("userFactory")
@RequiredArgsConstructor
public class UserFactory {

    public User makeUser(UserToRegister userToRegister) {
        String newUsername = userToRegister.getUsername();
        String newEmail = userToRegister.getEmail();
        String newPassword = userToRegister.getPassword();
        Set<Role> newRoles = getUserRoles();

        return new User(newUsername, newEmail, newPassword, newRoles);
    }


    private Set<Role> getUserRoles() {
        return new HashSet<>() {{
            add(Role.USER);
        }};
    }
}

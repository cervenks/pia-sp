package cz.cervenks.pia.business.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;

@Data
@AllArgsConstructor
public class Message {

    private String senderEmail;
    private String receiverEmail;
    private Timestamp timestamp;
    private String text;
}

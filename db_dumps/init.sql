--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE root;
ALTER ROLE root WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS PASSWORD 'SCRAM-SHA-256$4096:xkMev0Du3eZNo9Go2aOJXA==$FrdzWqeX/hnjH6Lh4ufiyt79mykDG3cFhjIcxVLVTxw=:nFwWUA7FWFQhXAzn2qvLzWn6vyvX0gccpPDbOz8b628=';






--
-- Databases
--

--
-- Database "template1" dump
--

\connect template1

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- PostgreSQL database dump complete
--

--
-- Database "pia_sp" dump
--

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: pia_sp; Type: DATABASE; Schema: -; Owner: root
--

CREATE DATABASE pia_sp WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'en_US.utf8';


ALTER DATABASE pia_sp OWNER TO root;

\connect pia_sp

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: auth_role; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.auth_role (
    id uuid NOT NULL,
    title character varying(20) NOT NULL
);


ALTER TABLE public.auth_role OWNER TO root;

--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.auth_user (
    id uuid NOT NULL,
    username character varying(100) NOT NULL,
    email character varying(200) NOT NULL,
    password character varying(200)
);


ALTER TABLE public.auth_user OWNER TO root;

--
-- Name: auth_userrole; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.auth_userrole (
    user_id uuid NOT NULL,
    role_id uuid NOT NULL
);


ALTER TABLE public.auth_userrole OWNER TO root;

--
-- Name: message; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.message (
    id uuid NOT NULL,
    sender_id uuid NOT NULL,
    receiver_id uuid NOT NULL,
    text text NOT NULL,
    "timestamp" timestamp without time zone NOT NULL
);


ALTER TABLE public.message OWNER TO root;

--
-- Name: post; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.post (
    id uuid NOT NULL,
    author_id uuid NOT NULL,
    text text NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    is_announcement boolean NOT NULL
);


ALTER TABLE public.post OWNER TO root;

--
-- Name: post_like; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.post_like (
    id uuid NOT NULL,
    post_id uuid NOT NULL,
    user_id uuid NOT NULL
);


ALTER TABLE public.post_like OWNER TO root;

--
-- Name: relationship; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.relationship (
    id uuid NOT NULL,
    user1_id uuid NOT NULL,
    user2_id uuid NOT NULL,
    state uuid NOT NULL
);


ALTER TABLE public.relationship OWNER TO root;

--
-- Name: relationship_state; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.relationship_state (
    id uuid NOT NULL,
    title character varying(100) NOT NULL
);


ALTER TABLE public.relationship_state OWNER TO root;

--
-- Data for Name: auth_role; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.auth_role (id, title) FROM stdin;
ef5b4418-200d-45dc-ad2c-620252b9389f	USER
fd717dad-390b-4df4-82e9-cc571e4a3a4f	ADMIN
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.auth_user (id, username, email, password) FROM stdin;
1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	pepa	pepa@piasp.com	$2a$10$Od4u18XwfQ3tadovDIoCG.j2rgr1bd3r4a57lNZwurrP5u2Hr0eB2
d05f3116-1e95-45cc-a096-ef943a34ee0a	honza	honza@piasp.com	$2a$10$VkZHaE0/VNUqrCv9Y0P3o.jHCPudvaI5tC6.2faQYR1GnMZqjUCum
e8a3e60a-2009-4324-ae0a-6f3b9e663913	jarda	jarda@piasp.com	$2a$10$dGjG300wqb.FzDSszTczaeH0nuzBHuJszTwmG5/ZRYkgBRiET3C/.
eab6c37d-87f2-48b5-a4fb-f4270edcffa8	superadmin	superadmin@admin.com	$2a$10$QAaIOR/IekcIwrWv8rY5PePEU.Zxzdsv1wIiMZWVZGVT2xWQ2ZWWW
\.


--
-- Data for Name: auth_userrole; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.auth_userrole (user_id, role_id) FROM stdin;
1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	ef5b4418-200d-45dc-ad2c-620252b9389f
d05f3116-1e95-45cc-a096-ef943a34ee0a	ef5b4418-200d-45dc-ad2c-620252b9389f
e8a3e60a-2009-4324-ae0a-6f3b9e663913	ef5b4418-200d-45dc-ad2c-620252b9389f
eab6c37d-87f2-48b5-a4fb-f4270edcffa8	ef5b4418-200d-45dc-ad2c-620252b9389f
eab6c37d-87f2-48b5-a4fb-f4270edcffa8	fd717dad-390b-4df4-82e9-cc571e4a3a4f
\.


--
-- Data for Name: message; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.message (id, sender_id, receiver_id, text, "timestamp") FROM stdin;
ddcb9373-504e-48b0-94f3-ffb5afb7ad2f	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	e8a3e60a-2009-4324-ae0a-6f3b9e663913	Ahoj	2022-01-15 17:33:26.97576
d446b2f7-a139-4da5-8bc3-a64be479e696	e8a3e60a-2009-4324-ae0a-6f3b9e663913	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	Posílám taky zprávu	2022-01-15 17:33:36.584066
5add89ef-8947-4515-89b0-abb0acd3eb66	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	e8a3e60a-2009-4324-ae0a-6f3b9e663913	<button id="message_send_button" class="btn btn-warning">                                 Poslat</button>	2022-01-15 17:34:12.507287
\.


--
-- Data for Name: post; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.post (id, author_id, text, "timestamp", is_announcement) FROM stdin;
5ea05a76-03cf-4b5b-8e23-bb961c7e98ef	e8a3e60a-2009-4324-ae0a-6f3b9e663913	Sample text 1	2022-01-11 12:00:00	f
116da273-0b1a-4c33-a2ca-ee718a12328c	e8a3e60a-2009-4324-ae0a-6f3b9e663913	Text 2	2022-01-11 12:30:00	f
46972924-d3fb-4ecc-83a7-3ce665fb82dc	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	Nový příspěvek.	2022-01-12 13:38:54.129523	f
5fdf37f8-4e55-4ac8-9569-f9251afcea09	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	Nový příspěvek.	2022-01-12 13:39:46.136024	f
d1db8fd4-975d-4141-8b8d-59ce7d9cf925	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	Příspěvek 3	2022-01-12 13:41:03.684797	f
99564d0e-a49b-47f4-8bee-9e0176d58186	e8a3e60a-2009-4324-ae0a-6f3b9e663913	Nový příspěvek XYZ	2022-01-12 13:43:38.849122	f
4f7611a3-1b6b-49f4-b2f5-5b7c23dcccc9	e8a3e60a-2009-4324-ae0a-6f3b9e663913	Nový příspěvek ABCDEF	2022-01-12 13:44:21.70975	f
863dd7bf-c3c4-4a7b-b9da-956ddee32a0a	d05f3116-1e95-45cc-a096-ef943a34ee0a	Úplně nový příspěvek	2022-01-12 13:51:21.224022	f
15ee1c6b-4cb0-44bd-8011-c7a1624683ce	d05f3116-1e95-45cc-a096-ef943a34ee0a	Další příspěvek ode mě	2022-01-12 13:53:50.182707	f
ef3de005-07c3-4962-8983-70e9994c0c95	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	Zdravím všechny	2022-01-12 13:54:32.222654	f
7762b6b4-9441-4e67-bf30-327fe203b3e0	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	Nový příspěvek	2022-01-12 13:55:37.702231	f
de54e244-5961-4a6b-8263-8675d7f7e05f	d05f3116-1e95-45cc-a096-ef943a34ee0a	ahoj	2022-01-12 14:02:28.125568	f
aad496b6-ce49-4d95-aec1-82f3b290c4dc	e8a3e60a-2009-4324-ae0a-6f3b9e663913	Příspěvek XYZ	2022-01-12 14:36:08.346649	f
262006ba-c282-4964-8c7a-2dbefcd87d45	e8a3e60a-2009-4324-ae0a-6f3b9e663913	Příspěvek ABC	2022-01-12 14:39:14.71203	f
50a4b50d-4687-41f6-a5ae-8c6d0ba13ca5	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	Test	2022-01-12 14:39:38.222272	f
7f9c3093-3134-4cfc-8354-2af2de0fd6a1	eab6c37d-87f2-48b5-a4fb-f4270edcffa8	Oznámení	2022-01-15 17:32:13.148661	t
38fc3de9-c121-4873-b9f8-85cc185d4518	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	<button id="message_send_button" class="btn btn-warning">\n                                Poslat</button>	2022-01-15 17:34:26.723863	f
\.


--
-- Data for Name: post_like; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.post_like (id, post_id, user_id) FROM stdin;
731bceaa-c5ff-4624-b71d-8b00b857384b	5ea05a76-03cf-4b5b-8e23-bb961c7e98ef	d05f3116-1e95-45cc-a096-ef943a34ee0a
eacffe75-e490-40ca-aaed-997becf4443d	116da273-0b1a-4c33-a2ca-ee718a12328c	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2
08ad4bd5-9faa-4370-857c-975553341d8e	d1db8fd4-975d-4141-8b8d-59ce7d9cf925	e8a3e60a-2009-4324-ae0a-6f3b9e663913
cb875dc1-a745-41e6-a88b-cd31f40f5b9a	d1db8fd4-975d-4141-8b8d-59ce7d9cf925	d05f3116-1e95-45cc-a096-ef943a34ee0a
fef5e31a-0dd9-42d8-a4fd-594be71be461	ef3de005-07c3-4962-8983-70e9994c0c95	d05f3116-1e95-45cc-a096-ef943a34ee0a
50a54a8e-a2c0-4540-8283-dfc6cc225de6	15ee1c6b-4cb0-44bd-8011-c7a1624683ce	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2
668fe0f2-c829-4bef-8825-dec98a0fd0c8	863dd7bf-c3c4-4a7b-b9da-956ddee32a0a	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2
38bcbef9-6b85-4fc6-b972-52636058be0f	7f9c3093-3134-4cfc-8354-2af2de0fd6a1	e8a3e60a-2009-4324-ae0a-6f3b9e663913
\.


--
-- Data for Name: relationship; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.relationship (id, user1_id, user2_id, state) FROM stdin;
fd4d7003-075c-4ab5-a46e-1405207c671f	d05f3116-1e95-45cc-a096-ef943a34ee0a	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	bf3677aa-90a2-4a93-a3ba-a960f88123aa
dfa00043-4a3c-4289-9715-ff0bfdf990a0	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	d05f3116-1e95-45cc-a096-ef943a34ee0a	bf3677aa-90a2-4a93-a3ba-a960f88123aa
7f864fad-bded-4ce4-96dc-9c0538f05a8a	e8a3e60a-2009-4324-ae0a-6f3b9e663913	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	bf3677aa-90a2-4a93-a3ba-a960f88123aa
8af1351a-9178-4c0a-93d2-b57a8d381dda	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	e8a3e60a-2009-4324-ae0a-6f3b9e663913	bf3677aa-90a2-4a93-a3ba-a960f88123aa
71c54618-4973-480b-84c1-02616f4da4b5	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	eab6c37d-87f2-48b5-a4fb-f4270edcffa8	bf3677aa-90a2-4a93-a3ba-a960f88123aa
16b1637b-e220-413d-b143-ef2651f7d5a8	eab6c37d-87f2-48b5-a4fb-f4270edcffa8	1a36716d-69d5-41e8-9ab4-42a2f2b1dad2	bf3677aa-90a2-4a93-a3ba-a960f88123aa
0a6724f5-ee52-4896-9561-ac3c4124e8b8	eab6c37d-87f2-48b5-a4fb-f4270edcffa8	e8a3e60a-2009-4324-ae0a-6f3b9e663913	36c9659e-42db-4cb1-afd5-c37c67b5bf0d
1f2a68d2-da1a-4ab6-9d26-a7cfdc44acd6	e8a3e60a-2009-4324-ae0a-6f3b9e663913	eab6c37d-87f2-48b5-a4fb-f4270edcffa8	da9ec12b-24c4-454c-9cda-011bd118e283
\.


--
-- Data for Name: relationship_state; Type: TABLE DATA; Schema: public; Owner: root
--

COPY public.relationship_state (id, title) FROM stdin;
da9ec12b-24c4-454c-9cda-011bd118e283	REQUESTED_BY_HIM
069d887f-1a99-43c8-80bf-de02a14d52c7	BLOCKED_BY_ME
bf3677aa-90a2-4a93-a3ba-a960f88123aa	FRIENDS
36c9659e-42db-4cb1-afd5-c37c67b5bf0d	REQUESTED_BY_ME
e49ba9a5-dc34-4136-b050-c12199c3eac0	BLOCKED_BY_HIM
\.


--
-- Name: auth_role auth_role_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.auth_role
    ADD CONSTRAINT auth_role_pkey PRIMARY KEY (id);


--
-- Name: auth_role auth_role_title_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.auth_role
    ADD CONSTRAINT auth_role_title_key UNIQUE (title);


--
-- Name: auth_user auth_user_email_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_email_key UNIQUE (email);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_userrole auth_userrole_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.auth_userrole
    ADD CONSTRAINT auth_userrole_pkey PRIMARY KEY (user_id, role_id);


--
-- Name: message message_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: post_like post_like_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.post_like
    ADD CONSTRAINT post_like_pkey PRIMARY KEY (id);


--
-- Name: post_like post_like_post_id_user_id_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.post_like
    ADD CONSTRAINT post_like_post_id_user_id_key UNIQUE (post_id, user_id);


--
-- Name: post post_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT post_pkey PRIMARY KEY (id);


--
-- Name: relationship relationship_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.relationship
    ADD CONSTRAINT relationship_pkey PRIMARY KEY (id);


--
-- Name: relationship_state relationship_state_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.relationship_state
    ADD CONSTRAINT relationship_state_pkey PRIMARY KEY (id);


--
-- Name: relationship_state relationship_state_title_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.relationship_state
    ADD CONSTRAINT relationship_state_title_key UNIQUE (title);


--
-- Name: relationship relationship_user1_id_user2_id_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.relationship
    ADD CONSTRAINT relationship_user1_id_user2_id_key UNIQUE (user1_id, user2_id);


--
-- Name: post author; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT author FOREIGN KEY (author_id) REFERENCES public.auth_user(id) NOT VALID;


--
-- Name: post_like post; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.post_like
    ADD CONSTRAINT post FOREIGN KEY (post_id) REFERENCES public.post(id) NOT VALID;


--
-- Name: message receiver; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT receiver FOREIGN KEY (receiver_id) REFERENCES public.auth_user(id) NOT VALID;


--
-- Name: auth_userrole role_userrole; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.auth_userrole
    ADD CONSTRAINT role_userrole FOREIGN KEY (role_id) REFERENCES public.auth_role(id) NOT VALID;


--
-- Name: message sender; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT sender FOREIGN KEY (sender_id) REFERENCES public.auth_user(id) NOT VALID;


--
-- Name: relationship state; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.relationship
    ADD CONSTRAINT state FOREIGN KEY (state) REFERENCES public.relationship_state(id) NOT VALID;


--
-- Name: post_like user; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.post_like
    ADD CONSTRAINT "user" FOREIGN KEY (user_id) REFERENCES public.auth_user(id) NOT VALID;


--
-- Name: relationship user1; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.relationship
    ADD CONSTRAINT user1 FOREIGN KEY (user1_id) REFERENCES public.auth_user(id) NOT VALID;


--
-- Name: relationship user2; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.relationship
    ADD CONSTRAINT user2 FOREIGN KEY (user2_id) REFERENCES public.auth_user(id) NOT VALID;


--
-- Name: auth_userrole user_userrole; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.auth_userrole
    ADD CONSTRAINT user_userrole FOREIGN KEY (user_id) REFERENCES public.auth_user(id) NOT VALID;


--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

\connect postgres

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--


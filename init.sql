
\connect pia_sp

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: auth_role; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.auth_role (
    id uuid NOT NULL,
    title character varying(20) NOT NULL
);


ALTER TABLE public.auth_role OWNER TO root;

--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.auth_user (
    id uuid NOT NULL,
    username character varying(100) NOT NULL,
    email character varying(200) NOT NULL,
    password character varying(200)
);


ALTER TABLE public.auth_user OWNER TO root;

--
-- Name: auth_userrole; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.auth_userrole (
    user_id uuid NOT NULL,
    role_id uuid NOT NULL
);


ALTER TABLE public.auth_userrole OWNER TO root;

--
-- Name: message; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.message (
    id uuid NOT NULL,
    sender_id uuid NOT NULL,
    receiver_id uuid NOT NULL,
    text text NOT NULL,
    "timestamp" timestamp without time zone NOT NULL
);


ALTER TABLE public.message OWNER TO root;

--
-- Name: post; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.post (
    id uuid NOT NULL,
    author_id uuid NOT NULL,
    text text NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    is_announcement boolean NOT NULL
);


ALTER TABLE public.post OWNER TO root;

--
-- Name: post_like; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.post_like (
    id uuid NOT NULL,
    post_id uuid NOT NULL,
    user_id uuid NOT NULL
);


ALTER TABLE public.post_like OWNER TO root;

--
-- Name: relationship; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.relationship (
    id uuid NOT NULL,
    user1_id uuid NOT NULL,
    user2_id uuid NOT NULL,
    state uuid NOT NULL
);


ALTER TABLE public.relationship OWNER TO root;

--
-- Name: relationship_state; Type: TABLE; Schema: public; Owner: root
--

CREATE TABLE public.relationship_state (
    id uuid NOT NULL,
    title character varying(100) NOT NULL
);


ALTER TABLE public.relationship_state OWNER TO root;

--
-- Data for Name: auth_role; Type: TABLE DATA; Schema: public; Owner: root
--


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: root
--

--
-- Data for Name: auth_userrole; Type: TABLE DATA; Schema: public; Owner: root
--

--
-- Name: auth_role auth_role_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.auth_role
    ADD CONSTRAINT auth_role_pkey PRIMARY KEY (id);


--
-- Name: auth_role auth_role_title_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.auth_role
    ADD CONSTRAINT auth_role_title_key UNIQUE (title);


--
-- Name: auth_user auth_user_email_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_email_key UNIQUE (email);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_userrole auth_userrole_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.auth_userrole
    ADD CONSTRAINT auth_userrole_pkey PRIMARY KEY (user_id, role_id);


--
-- Name: message message_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT message_pkey PRIMARY KEY (id);


--
-- Name: post_like post_like_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.post_like
    ADD CONSTRAINT post_like_pkey PRIMARY KEY (id);


--
-- Name: post_like post_like_post_id_user_id_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.post_like
    ADD CONSTRAINT post_like_post_id_user_id_key UNIQUE (post_id, user_id);


--
-- Name: post post_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT post_pkey PRIMARY KEY (id);


--
-- Name: relationship relationship_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.relationship
    ADD CONSTRAINT relationship_pkey PRIMARY KEY (id);


--
-- Name: relationship_state relationship_state_pkey; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.relationship_state
    ADD CONSTRAINT relationship_state_pkey PRIMARY KEY (id);


--
-- Name: relationship_state relationship_state_title_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.relationship_state
    ADD CONSTRAINT relationship_state_title_key UNIQUE (title);


--
-- Name: relationship relationship_user1_id_user2_id_key; Type: CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.relationship
    ADD CONSTRAINT relationship_user1_id_user2_id_key UNIQUE (user1_id, user2_id);


--
-- Name: post author; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT author FOREIGN KEY (author_id) REFERENCES public.auth_user(id) NOT VALID;


--
-- Name: post_like post; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.post_like
    ADD CONSTRAINT post FOREIGN KEY (post_id) REFERENCES public.post(id) NOT VALID;


--
-- Name: message receiver; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT receiver FOREIGN KEY (receiver_id) REFERENCES public.auth_user(id) NOT VALID;


--
-- Name: auth_userrole role_userrole; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.auth_userrole
    ADD CONSTRAINT role_userrole FOREIGN KEY (role_id) REFERENCES public.auth_role(id) NOT VALID;


--
-- Name: message sender; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.message
    ADD CONSTRAINT sender FOREIGN KEY (sender_id) REFERENCES public.auth_user(id) NOT VALID;


--
-- Name: relationship state; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.relationship
    ADD CONSTRAINT state FOREIGN KEY (state) REFERENCES public.relationship_state(id) NOT VALID;


--
-- Name: post_like user; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.post_like
    ADD CONSTRAINT "user" FOREIGN KEY (user_id) REFERENCES public.auth_user(id) NOT VALID;


--
-- Name: relationship user1; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.relationship
    ADD CONSTRAINT user1 FOREIGN KEY (user1_id) REFERENCES public.auth_user(id) NOT VALID;


--
-- Name: relationship user2; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.relationship
    ADD CONSTRAINT user2 FOREIGN KEY (user2_id) REFERENCES public.auth_user(id) NOT VALID;


--
-- Name: auth_userrole user_userrole; Type: FK CONSTRAINT; Schema: public; Owner: root
--

ALTER TABLE ONLY public.auth_userrole
    ADD CONSTRAINT user_userrole FOREIGN KEY (user_id) REFERENCES public.auth_user(id) NOT VALID;


--
-- PostgreSQL database dump complete
--

--
-- Database "postgres" dump
--

\connect postgres

--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1 (Debian 14.1-1.pgdg110+1)
-- Dumped by pg_dump version 14.1 (Debian 14.1-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

